package ennova.simagente;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static ennova.simagente.ExpListViewAdapterWithCheckboxMessage.simChecked;
import static ennova.simagente.LoginActivity.KEY_GRANT_TYPE;
import static ennova.simagente.LoginActivity.TOKEN_KEY;
import static ennova.simagente.LoginActivity.URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {

    private String MESSAGE_URL = URL + "service/apiv1/message?access_token=" + TOKEN_KEY;
    private String ID              =   "id";
    private ExpandableListView listView;
    private ExpListViewAdapterWithCheckboxMessage adapter;
    private ArrayList<String> messageSelected = new ArrayList<>();
    private List<String> cleanHeadersList = null;
    private HashMap<String, List<String>> listDataChildMessage;
    private ArrayList<String> cleanHeadersListMessage = null;
    private View mProgressView;

    public static NotificationFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        NotificationFragment notificationFragment = new NotificationFragment();
        notificationFragment.setArguments(args);
        return notificationFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notification_inbox, container, false);
        ((MainActivity) getActivity()).setActionBarTitle("Messaggi");
        ((MainActivity)getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()){
            public void doBack() {
                if(!simChecked.isEmpty()){
                    MESSAGE_URL = URL + "service/apiv1/message?access_token=" + TOKEN_KEY;
                    StringRequest stringRequestCancel = new StringRequest(Request.Method.POST, MESSAGE_URL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    if(response.contains("updated")){
                                        /** Create new fragment and transaction */
                                        Fragment newFragment = new GiacenzaFragment();
                                        /** consider using Java coding conventions (upper first char class names!!!) */
                                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                        /** sending arguments to another fragment*/
                                        //Bundle args = new Bundle();
                                        //args.putStringArrayList("simSelected", simChecked);
                                        //newFragment.setArguments(args);
                                        /** Replace whatever is in the fragment_container view with this fragment,
                                         * and add the transaction to the back stack */
                                        transaction.replace(R.id.container, newFragment);
                                        //transaction.addToBackStack(null);
                                        /** Commit the transaction */
                                        transaction.commit();
                                    }else{
                                        Toast.makeText(getActivity(),"Impossibile aggiornare le segnalazione",Toast.LENGTH_SHORT ).show();
                                    }
                                    //showProgress(false);
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getActivity(),"Impossibile aggiornare le segnalazione",Toast.LENGTH_SHORT ).show();
                                    //showProgress(false);
                                }
                            }){

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> map = new HashMap<>();
                            map.put(KEY_GRANT_TYPE,"password");// necessary for webservice
                            for(int i=0; i < simChecked.size(); i++) {
                                map.put("id["+i+"]",String.valueOf(simChecked.get(i)));
                            }
                            return map;
                        }
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<>();
                            headers.put("X-HTTP-Method-Override", "PATCH");
                            return headers;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                    requestQueue.add(stringRequestCancel);
                } else {
                    /** Create new fragment and transaction */
                    Fragment newFragment = new GiacenzaFragment();
                    /** consider using Java coding conventions (upper first char class names!!!) */
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    /** sending arguments to another fragment*/
                    //Bundle args = new Bundle();
                    //args.putStringArrayList("simSelected", simChecked);
                    //newFragment.setArguments(args);
                    /** Replace whatever is in the fragment_container view with this fragment,
                     * and add the transaction to the back stack */
                    transaction.replace(R.id.container, newFragment);
                    //transaction.addToBackStack(null);
                    /** Commit the transaction */
                    transaction.commit();
                }
            }});

        /** recupero la lista dal layout */
        listView = (ExpandableListView) view.findViewById(R.id.lvExpMessage_notification);

        mProgressView = view.findViewById(R.id.notification_progress);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, MESSAGE_URL,null,
                new Response.Listener<JSONObject>() {
                    ArrayList<String> messageList =  new ArrayList<>();
                    ArrayList<String> messageHeaderList =  new ArrayList<>();
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray myMessages = response.getJSONArray("detail");
                            /** looping through All Messages */
                            for (int i = 0; i < myMessages.length(); i++) {
                                JSONObject c = myMessages.getJSONObject(i);

                                /** detail node is JSON Object */
                                String id_message = c.getString("id");
                                String read_message = c.getString("read");
                                String text_message = c.getString("text");
                                String header = c.getString("day");

                                /** adding messages to listData */
                               // String text_message_short = text_message.substring(0, Math.min(text_message.length(), 70))+ " ...";
                                messageList.add("("+header+")"+"["+id_message+"]"+text_message);
                                messageHeaderList.add(header);
                            }
                            /** Header, Child data */
                            prepareListDataMessaggi(messageHeaderList,messageList);
                            adapter = new ExpListViewAdapterWithCheckboxMessage(getActivity(), cleanHeadersListMessage, listDataChildMessage);
                            /** inietto i dati */
                            listView.setAdapter(adapter);
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(),"Impossibile aggiornare le segnalzione",Toast.LENGTH_SHORT ).show();
                        }
                        showProgress(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        Toast.makeText(getActivity(),"Impossibile aggiornare le segnalazione",Toast.LENGTH_SHORT ).show();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.getActivity());
        requestQueue.add(jsonRequest);
        showProgress(true);

        /** Listview on child click listener */
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                CheckBox checkBox = (CheckBox)v.findViewById(R.id.lstcheckBoxMessage);
                checkBox.setChecked(!checkBox.isChecked());
                checkBox.setVisibility((!checkBox.isChecked())?View.GONE:View.VISIBLE);
                return true;
            }
        });

        /** Inflate the layout for this fragment */
        return view;
    }

    /**
     * Preparing the list data
     */
    private void prepareListDataMessaggi(ArrayList<String> messageHeaderList, ArrayList<String> messageList) {

        listDataChildMessage = new HashMap<>();

        cleanHeadersListMessage = messageHeaderList;
        Set<String> hs = new HashSet<>(messageHeaderList);
        hs.addAll(cleanHeadersListMessage);
        cleanHeadersListMessage.clear();
        cleanHeadersListMessage.addAll(hs);
        Collections.sort(cleanHeadersListMessage,Collections.reverseOrder());

        for(int n = 0; n < cleanHeadersListMessage.size(); n++){
            ArrayList cleanMessageList = new ArrayList<>();
            for(int p=0;p<messageList.size(); p++){
                String sCustomer = messageList.get(p);
                String indexHeader = cleanHeadersListMessage.get(n);
                String oneMatch = sCustomer.substring(sCustomer.indexOf("(") + 1, sCustomer.indexOf(")"));
                String serialNumber = sCustomer.substring(sCustomer.lastIndexOf(")") + 1);
                if(oneMatch.equals(indexHeader)){
                    cleanMessageList.add(serialNumber);
                }
            }
            listDataChildMessage.put(cleanHeadersListMessage.get(n), cleanMessageList);
        }
        for(String key : listDataChildMessage.keySet()){
            Collections.sort(listDataChildMessage.get(key));
        }
    }

    /**
     * Shows the progress UI
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        /** On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
         * for very easy animations. If available, use these APIs to fade-in
         * the progress spinner. */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            listView.setVisibility(show ? View.GONE : View.VISIBLE);
            listView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    listView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            /** The ViewPropertyAnimator APIs are not available, so simply show
             *  and hide the relevant UI components. */
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            listView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
