package ennova.simagente;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static ennova.simagente.LoginActivity.KEY_GRANT_TYPE;
import static ennova.simagente.LoginActivity.TOKEN_KEY;
import static ennova.simagente.LoginActivity.URL;

public class LettoreConsegnaFragment extends Fragment {
    private String CONFERMA_AGENZIA_URL = URL +
            "service/apiv1/movement?access_token=" + TOKEN_KEY;

    private HashMap<String, String> simListConsegnaObject = new HashMap<>();
    private ArrayList<String> cleanSimScannedAndOrdered;
    private ArrayList<String> simSelectedDaConsegnare = new ArrayList<>();
    private ArrayList<String> simRimasteConsegnaCliente = new ArrayList<>();
    private ArrayList simRimasteAgenzia = new ArrayList<>();

    public static LettoreConsegnaFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        LettoreConsegnaFragment lettoreConsegnaFragment = new LettoreConsegnaFragment();
        lettoreConsegnaFragment.setArguments(args);
        return lettoreConsegnaFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lettore_consegna, container, false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Seleziona destinatario");
        /** get arguments */
        Bundle b = this.getArguments();
        simRimasteConsegnaCliente = getArguments().getStringArrayList("simRimasteConsegnaCliente");
        simListConsegnaObject = (HashMap<String,String>)b.getSerializable("simListConsegnaObject");
        simRimasteAgenzia = getArguments().getStringArrayList("simSelected");

        if(simRimasteAgenzia!=null){
            simRimasteConsegnaCliente=simRimasteAgenzia;
        }

        /** The checkbox for the each item is specified by the layout
         * android.R.layout.simple_list_item_multiple_choice
         */
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this.getActivity(), android.R.layout.simple_list_item_multiple_choice, getSimScanned());

        /** Getting the reference to the listview object of the layout */
        final ListView listView = (ListView) view.findViewById(R.id.sim_scanner);

        /** Setting adapter to the listview */
        listView.setAdapter(adapter);

        /** setting all checkbox to checked status*/
        for ( int i=0; i< adapter.getCount(); i++ ) {
            listView.setItemChecked(i, true);
        }

        Button consegnaAgenzia = (Button) view.findViewById(R.id.consegna_agenzia);
        consegnaAgenzia.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SparseBooleanArray checked = listView.getCheckedItemPositions();
                /** number of name-value pairs in the array */
                int size = checked.size();
                for (int i = 0; i < size; i++) {
                    int key = checked.keyAt(i);
                    boolean value = checked.get(key);
                    if (value){
                        simSelectedDaConsegnare.add(String.valueOf(listView.getItemAtPosition(i)));
                    }
                }
                AlertDialog alertDialogConfirm = new AlertDialog.Builder(getActivity()).create();
                alertDialogConfirm.setMessage("Sicuro di voler consegnare le SIM selezionate in agenzia?");
                alertDialogConfirm.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /** webservice request */
                        StringRequest stringRequestConfermaAgenzia = new StringRequest(Request.Method.POST, CONFERMA_AGENZIA_URL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        if(response.contains("Movements")){
                                            AlertDialog alertDialogo = new AlertDialog.Builder(getActivity()).create();
                                            alertDialogo.setMessage("SIM consegnate correttamente!");
                                            alertDialogo.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    ArrayList<String> existSim;
                                                    existSim = getSimRimaste();
                                                    if(!existSim.isEmpty()){
                                                        /** Create new fragment and transaction */
                                                        Fragment newFragment = new LettoreConsegnaFragment();
                                                        /** consider using Java coding conventions (upper first char class names!!!) */
                                                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                                        /** sending arguments to another fragment*/
                                                        Bundle args = new Bundle();
                                                        args.putStringArrayList("simSelected", getSimRimaste());
                                                        newFragment.setArguments(args);
                                                        /** Replace whatever is in the fragment_container view with this fragment,
                                                         * and add the transaction to the back stack */
                                                        transaction.replace(R.id.container, newFragment);
                                                        transaction.addToBackStack(null);
                                                        /** Commit the transaction */
                                                        transaction.commit();
                                                    }else{
                                                        Intent intent = new Intent(getActivity(), MainActivity.class);
                                                        startActivity(intent);
                                                    }
                                                }});
                                            alertDialogo.show();
                                        }else{
                                            Toast.makeText(getActivity(),"Impossibile aggiornare le SIM",Toast.LENGTH_SHORT ).show();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getActivity(),"Impossibile aggiornare le SIM",Toast.LENGTH_SHORT ).show();
                                    }
                                }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String,String> map = new HashMap<>();
                                for(int i=0; i < simSelectedDaConsegnare.size(); i++) {
                                    map.put("sn["+i+"][0]", String.valueOf(simSelectedDaConsegnare.get(i)));
                                    map.put("sn["+i+"][1]","ATTIVABILE");
                                }
                                map.put(KEY_GRANT_TYPE,"password");// necessary for webservice
                                map.put("directory_id", "parent");
                                return map;
                            }
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> headers = new HashMap<>();
                                headers.put("X-HTTP-Method-Override", "PATCH");
                                return headers;
                            }
                        };
                        RequestQueue requestQueueConsegnaAgenzia = Volley.newRequestQueue(getActivity());
                        requestQueueConsegnaAgenzia.add(stringRequestConfermaAgenzia);
                    }});
                alertDialogConfirm.setButton(Dialog.BUTTON_NEGATIVE, "Annulla", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if ( getFragmentManager().getBackStackEntryCount() > 0)
                        {
                            /** Create new fragment and transaction */
                            Fragment newFragment = new LettoreFragment();
                            /** consider using Java coding conventions (upper first char class names!!!) */
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            /** sending arguments to another fragment*/
                            Bundle args = new Bundle();
                            //args.putStringArrayList("simSelected", getSimRimaste());
                            newFragment.setArguments(args);
                            /** Replace whatever is in the fragment_container view with this fragment,
                             * and add the transaction to the back stack */
                            transaction.replace(R.id.container, newFragment);
                            transaction.addToBackStack(null);
                            /** Commit the transaction */
                            transaction.commit();
                        }
                    } });
                alertDialogConfirm.show();

            }});

        Button consegnaCliente = (Button) view.findViewById(R.id.consegna_cliente);
        consegnaCliente.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (getFragmentManager().getBackStackEntryCount() > 0)
                {
                    SparseBooleanArray checkedCliente = listView.getCheckedItemPositions();
                    /** number of name-value pairs in the array */
                    int size = checkedCliente.size();
                    for (int i = 0; i < size; i++) {
                        int key = checkedCliente.keyAt(i);
                        boolean value = checkedCliente.get(key);
                        if (value){
                            simSelectedDaConsegnare.add(String.valueOf(listView.getItemAtPosition(i)));
                        }
                    }
                    /** Create new fragment and transaction */
                    Fragment newFragment = new ConsegnaClienteFragment();
                    /** consider using Java coding conventions (upper first char class names!!!) */
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    /** sending arguments to another fragment*/
                    Bundle args = new Bundle();
                    args.putStringArrayList("simSelected", simSelectedDaConsegnare);
                    args.putStringArrayList("simRimasteConsegnaCliente", getSimRimaste());
                    newFragment.setArguments(args);
                    /** Replace whatever is in the fragment_container view with this fragment,
                     * and add the transaction to the back stack */
                    transaction.replace(R.id.container, newFragment);
                    transaction.addToBackStack(null);
                    /** Commit the transaction */
                    transaction.commit();
                }

            }});

        return view;
    }

    /**
     * ArrayList of All scanned SIM
     * @return
     */
    private ArrayList getSimScanned(){
        if(simRimasteConsegnaCliente == null){
            /** Getting Set of keys */
            Set<String> keySet = simListConsegnaObject.keySet();
            /** Creating an ArrayList of keys */
            ArrayList<String> simScanned = new ArrayList<>(keySet);
            for (String key : simScanned)
            {
            }
            cleanSimScannedAndOrdered = simScanned;
            Set<String> hs = new HashSet<>(simScanned);
            hs.addAll(cleanSimScannedAndOrdered);
            cleanSimScannedAndOrdered.clear();
            cleanSimScannedAndOrdered.addAll(hs);
            Collections.sort(cleanSimScannedAndOrdered);

            return cleanSimScannedAndOrdered;
        }else{
            return simRimasteConsegnaCliente;
        }
    }

    /**
     * ArrayListof all status
     * @return
     */
    private ArrayList getStatus(){
        /** Getting Collection of values */
        Collection<String> values = simListConsegnaObject.values();

        /** Creating an ArrayList of values */
        ArrayList<String> simStatus = new ArrayList<>(values);

        for (String value : simStatus)
        {
        }

        return simStatus;
    }

    /**
     * ArrayListof all status
     * @return
     */
    private ArrayList getArrayListOfKeyValues(){
        /** Getting the Set of entries*/
        Set<Map.Entry<String, String>> entrySet = simListConsegnaObject.entrySet();

        /** Creating an ArrayList Of Entry objects */
        ArrayList<Map.Entry<String, String>> simSerialAndStatus = new ArrayList<>(entrySet);

        for (Map.Entry<String, String> entry : simSerialAndStatus)
        {
        }

        return simSerialAndStatus;
    }

    /**
     * compare two ArrayList and return those not checked
     * @return
     */
    private ArrayList<String> getSimRimaste(){
        ArrayList<String> simConsegnate = new ArrayList<>(simSelectedDaConsegnare);
        Collection<String> simConsegnateCollection = new ArrayList(simConsegnate);
        ArrayList<String> simRimaste = new ArrayList<>(getSimScanned());
        simRimaste.removeAll(simConsegnateCollection);

        return simRimaste;
    }
}
