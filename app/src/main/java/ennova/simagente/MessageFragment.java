package ennova.simagente;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import static ennova.simagente.LoginActivity.TOKEN_KEY;
import static ennova.simagente.LoginActivity.USER;
import static ennova.simagente.LoginActivity.URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends Fragment {

    private String MESSAGE_URL = URL + "service/apiv1/message?access_token=" + TOKEN_KEY;

    private String SUBJECT="subject";
    private String MESSAGE= "message";
    private String FROM= "from";

    private EditText editTextOggetto;
    private EditText editTextSegnalazione;
    private View mProgressView;
    private View mMessageFormView;
    private String oggetto;
    private String segnalazione;

    public static MessageFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        MessageFragment messageFragment = new MessageFragment();
        messageFragment.setArguments(args);
        return messageFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.message, container, false);
        ((MainActivity) getActivity()).setActionBarTitle("Segnalazione");
        setHasOptionsMenu(true);
        ((MainActivity)getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()){
            public void doBack() {

            }});
        /** hiding soft keyboard*/
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        /** form fields*/
        editTextOggetto = (EditText) view.findViewById(R.id.subject_message);
        editTextSegnalazione = (EditText) view.findViewById(R.id.text_mesage);
        mMessageFormView = view.findViewById(R.id.new_message_form);
        mProgressView = view.findViewById(R.id.send_progress);
        editTextOggetto.setText(null);
        editTextSegnalazione.setText(null);
        /** send message by floating button */
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.send_message);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newMessage(view);
            }
        });
        /** Inflate the layout for this fragment */
        return view;
    }
    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.logout_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /** Handle item selection */
        switch (item.getItemId()) {
            case R.id.logout_menu:
                getActivity().finish();
                Intent intent = new Intent(this.getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void newMessage(View view) {
        /** hiding soft keyboard*/
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        oggetto = editTextOggetto.getText().toString().trim();
        segnalazione = editTextSegnalazione.getText().toString().trim();
        AlertDialog alertDialogo = new AlertDialog.Builder(getActivity()).create();
        alertDialogo.setMessage("Inviare il messaggio?");
        alertDialogo.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                StringRequest stringRequest = new StringRequest(Request.Method.POST, MESSAGE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("Messages")){
                            AlertDialog alertDialogo = new AlertDialog.Builder(getActivity()).create();
                            alertDialogo.setTitle("Segnalazione inviata!");
                            alertDialogo.setIcon(android.R.drawable.ic_dialog_info);
                            alertDialogo.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    /** Create new fragment and transaction */
                                    Fragment messageFragment = new MessageFragment();
                                    /** consider using Java coding conventions (upper first char class names!!!) */
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    /** Replace whatever is in the fragment_container view with this fragment,
                                     * and add the transaction to the back stack */
                                    transaction.replace(R.id.container, messageFragment);
                                    //transaction.addToBackStack(null);
                                    /** Commit the transaction */
                                    transaction.commit();
                                }});
                            alertDialogo.show();
                        }else{
                            Toast.makeText(getActivity(),"Impossibile inviare la segnalazione",Toast.LENGTH_SHORT).show();
                        }
                        showProgress(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(),"Impossibile inviare la segnalazione",Toast.LENGTH_SHORT ).show();
                        showProgress(false);
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put(SUBJECT,oggetto);
                map.put(MESSAGE,segnalazione);
                map.put(FROM,USER);
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
        showProgress(true);
        }});
        alertDialogo.setButton(Dialog.BUTTON_NEGATIVE, "Annulla", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            if ( getFragmentManager().getBackStackEntryCount() > 0)
            {
                getFragmentManager().popBackStack();
                return;
            }
        }});
        alertDialogo.show();
    }

    /**
     * Shows the progress UI and hides the message form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        /** On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
         * for very easy animations. If available, use these APIs to fade-in
         * the progress spinner. */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mMessageFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mMessageFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mMessageFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            /** The ViewPropertyAnimator APIs are not available, so simply show
             *  and hide the relevant UI components. */
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mMessageFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
    @Override
    public void onStart(){
        super.onStart();
        ((MainActivity) getActivity()).setActionBarTitle("Segnalazione");
        editTextOggetto.setText(null);
        editTextSegnalazione.setText(null);
    }
    @Override
    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Segnalazione");
        editTextOggetto.setText(null);
        editTextSegnalazione.setText(null);
    }
}
