package ennova.simagente;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    //public static final String URL = "http://aradia-nlp.dev-ennova.com/develop/gestione_system_b2b/web/";
    public static final String URL = "https://app-vodafone.ennova.it/gestione-b2b/";
    public static final String LOGIN_URL = URL + "user/oauth/token";


    public static String PREFERENCES = "MyPrefs";
    public static String PREF_USERNAME="username";
    public static String PREF_PASSWORD="password";
    public static String REMEMBER_ME="remember";
    private SwitchCompat  saveLoginSwitch;

    public static final String KEY_TEST_CLIENT="androclient";
    public static final String KEY_TEST_PASS="andropass";
    public static final String KEY_GRANT_TYPE="grant_type";
    public static final String KEY_USERNAME="username";
    public static final String KEY_PASSWORD="password";
    public static final String ACCESS_TOKEN="access_token";
    public static final String ACCESS_TOKEN_PREF ="";
    public static final String EXPIRES_IN = "";
    public static final String TOKEN_TYPE = "";
    public static final String SCOPE = "";
    public static final String REFRESH_TOKEN = "";
    public static String USER = "USER";
    SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    public static String TOKEN_KEY="";

    private EditText editTextUsername;
    private EditText editTextPassword;
    private Button buttonLogin;

    private String username;
    private String password;
    private String usernamePref;
    private String passwordPref;
    private Boolean rememberPref;

    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        /*VideoView mVideoView = (VideoView)findViewById(R.id.videoView);
        mVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.vodafonevid));
        mVideoView.requestFocus();
        mVideoView.start();*/
        /** read username and password from SharedPreferences */
        getUser();

        editTextUsername = (EditText) findViewById(R.id.user);
        editTextPassword = (EditText) findViewById(R.id.password);
        saveLoginSwitch = (SwitchCompat)findViewById(R.id.saveLoginCheckBox);

        editTextUsername.setText(usernamePref);
        editTextPassword.setText(passwordPref);
        saveLoginSwitch.setChecked((Boolean.TRUE.equals(rememberPref))? rememberPref : Boolean.FALSE);

        buttonLogin = (Button) findViewById(R.id.action_sign_in_button);
        mLoginFormView = findViewById(R.id.action_sign_in_button);
        mProgressView = findViewById(R.id.login_progress);

        buttonLogin.setOnClickListener(this);
        /*mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });*/
    }


    private void userLogin() {

        username = editTextUsername.getText().toString().trim();
        password = editTextPassword.getText().toString().trim();
        USER =username.toUpperCase();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                            if(saveLoginSwitch.isChecked()){
                                /** save username, password and remember me status **/
                                rememberMe(username, password, "true");
                                userCredentials(response);
                            }
                            USER =username.toUpperCase();

                            Intent in = new Intent(LoginActivity.this,MainActivity.class);
                            startActivity(in);
                            showProgress(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if(isOnline()){
                            Toast.makeText(LoginActivity.this,"Utente o Password sbagliata",Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(LoginActivity.this,"Non c'è conessione al server o internet",Toast.LENGTH_LONG).show();
                        }
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put(KEY_GRANT_TYPE,"password");
                map.put(KEY_USERNAME,username);
                map.put(KEY_PASSWORD,password);
                return map;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                String creds = String.format("%s:%s",KEY_TEST_CLIENT,KEY_TEST_PASS);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                headers.put("Authorization", auth);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void openMain(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(KEY_USERNAME, username);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        userLogin();
        showProgress(true);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void userCredentials(String response) {
        String[] firstPart = response.split(",");
        String secondPart = firstPart[0];
        String[] credentials = secondPart.split(":");
        TOKEN_KEY = credentials[1].replace("\"", "");
    }

    public void onStart(){
        super.onStart();
        /** read username and password from SharedPreferences */
        getUser();
    }

    public void getUser(){
        SharedPreferences pref = getSharedPreferences(PREFERENCES,MODE_PRIVATE);
        usernamePref = pref.getString(PREF_USERNAME, null);
        passwordPref = pref.getString(PREF_PASSWORD, null);
        rememberPref = pref.getBoolean(REMEMBER_ME,false);

    }

    public void rememberMe(String user, String password, String remember){
        /** save username and password in SharedPreferences */
        getSharedPreferences(PREFERENCES,MODE_PRIVATE)
                .edit()
                .putString(PREF_USERNAME,user)
                .putString(PREF_PASSWORD,password)
                .putBoolean(REMEMBER_ME,true)
                .apply();
    }

    /**
     * performing a ping on server
     * @return
     */
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}