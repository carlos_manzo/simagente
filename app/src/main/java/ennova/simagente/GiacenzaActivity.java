package ennova.simagente;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class GiacenzaActivity extends AppCompatActivity {
    String[] sim = new String[]{
            "8939104700102037071",
            "8939104700102037072",
            "8939104700102037073",
            "8939104700102037074",
            "8939104700102037075",
            "8939104700102037076",
            "8939104700102037077",
            "8939104700102037078",
            "8939104700102037079",
            "8939104700102037080"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giacenza);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, sim);

        // Getting the reference to the listview object of the layout
        //ListView listView = (ListView) findViewById(R.id.lv_giacenza_cliente);

        // Setting adapter to the listview
        //listView.setAdapter(adapter);
    }
    public void onStart(){
        super.onStart();
        //TextView textGiacenza = (TextView) findViewById(R.id.toolbar_giacenza);
        //textGiacenza.setText("Queste le SIM in giacenza a " + getIntent().getStringExtra("customer"));
        //TextView ragione_sociale=(TextView)findViewById(R.id.text_giacenza);
        //show text in the Intent object in the TextView
        setTitle(getIntent().getStringExtra("customer"));

        //ragione_sociale.setText("Queste le SIM in giacenza a " + getIntent().getStringExtra("customer"));
        // The checkbox for the each item is specified by the layout android.R.layout.simple_list_item_multiple_choice

    }

    public void onBackPressed()
    {
        //do whatever you want the 'Back' button to do
        //as an example the 'Back' button is set to start a new Activity named 'NewActivity'
        this.startActivity(new Intent(GiacenzaActivity.this,MainActivity.class));

        return;
    }
}
