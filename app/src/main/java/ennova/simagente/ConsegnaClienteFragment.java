package ennova.simagente;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;

import static ennova.simagente.ExpListViewAdapterWithCheckbox.simChecked;

public class ConsegnaClienteFragment extends Fragment {
    private ArrayList simSelectedOnGiacenza = new ArrayList<>();
    private ArrayList simSelectedDaAttivare = new ArrayList<>();
    private ArrayList simSelectedDiScorta = new ArrayList<>();
    private ArrayList simRimasteConsegnaCliente = new ArrayList<>();

    public static ConsegnaClienteFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        ConsegnaClienteFragment consegnaClienteFragment = new ConsegnaClienteFragment();
        consegnaClienteFragment.setArguments(args);
        return consegnaClienteFragment;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.consegna_cliente, container, false);
        ((MainActivity) getActivity()).setActionBarTitle("Conferma selezione");
        simSelectedOnGiacenza = getArguments().getStringArrayList("simSelected");
        Collections.sort(simSelectedOnGiacenza);
        simRimasteConsegnaCliente = getArguments().getStringArrayList("simRimasteConsegnaCliente");
        if(simRimasteConsegnaCliente != null){
            Collections.sort(simRimasteConsegnaCliente);
        }
        //simSelectedOnGiacenza = (simRimasteConsegnaCliente == null)? simSelectedOnGiacenza : simRimasteConsegnaCliente;
        /** The checkbox for the each item is specified by the layout
         * android.R.layout.simple_list_item_multiple_choice
         */
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this.getActivity(), android.R.layout.simple_list_item_multiple_choice, simSelectedOnGiacenza);

        /**  Getting the reference to the listview object of the layout */
        final ListView listView = (ListView) view.findViewById(R.id.lvExp1);

        /** Setting adapter to the listview */
        listView.setAdapter(adapter);
        /** setting all checkbox to checked status*/
        for ( int i=0; i< adapter.getCount(); i++ ) {
            listView.setItemChecked(i, true);
        }

        Button buttonOk = (Button) view.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SparseBooleanArray checked = listView.getCheckedItemPositions();
                /** number of name-value pairs in the array */
                int size = checked.size();
                for (int i = 0; i < size; i++) {
                    int key = checked.keyAt(i);
                    boolean value = checked.get(key);
                    if (value){
                        simSelectedDaAttivare.add(String.valueOf(listView.getItemAtPosition(i)));
                    }else{
                        simSelectedDiScorta.add("(scorta)"+String.valueOf(listView.getItemAtPosition(i)));
                    }
                }
                /** Create new fragment and transaction */
                Fragment newFragment = new ClientiSimStockFragment();
                /** consider using Java coding conventions (upper first char class names!!!) */
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                /** sending arguments to another fragment*/
                Bundle args = new Bundle();
                args.putStringArrayList("simAttivare", simSelectedDaAttivare);
                args.putStringArrayList("simScorta", simSelectedDiScorta);
                args.putStringArrayList("simRimasteConsegnaCliente",simRimasteConsegnaCliente);
                newFragment.setArguments(args);
                /** Replace whatever is in the fragment_container view with this fragment,
                 * and add the transaction to the back stack */
                transaction.replace(R.id.container, newFragment);
                transaction.addToBackStack(null);
                /** Commit the transaction */
                transaction.commit();
            }});
        Button buttonAnulla = (Button) view.findViewById(R.id.buttonAnulla);
        buttonAnulla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if ( getFragmentManager().getBackStackEntryCount() > 0)
                {
                    getFragmentManager().popBackStack();
                    return;
                }

            }});

        return view;
    }
}
