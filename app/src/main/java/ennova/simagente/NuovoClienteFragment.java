package ennova.simagente;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import static ennova.simagente.LoginActivity.TOKEN_KEY;
import static ennova.simagente.LoginActivity.URL;

public class NuovoClienteFragment extends Fragment {

    public static final String NEW_CUSTOMER_URL = URL + "service/apiv1/customer?access_token=" + TOKEN_KEY;

    private String UID= "uid";
    private String DIRECTORY_ID ="directory_id";
    private String DISPLAY_NAME="display_name";
    private String STATUS= "status";

    private EditText editTextRagioneSociale;
    private EditText editTextContattoRiferimento;
    private EditText editTextNumeroContatto;
    private EditText editTextEmail;
    private EditText editTextPartitaIva;
    private EditText editTextSedeOperativa;

    private String ragioneSociale;
    private String contattoRiferimento;
    private String numeroContatto;
    private String email;
    private String partitaIva;
    private String sedeOperativa;

    private View mProgressView;
    private View mLoginFormView;

    public static NuovoClienteFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        NuovoClienteFragment clienteSelezzionatoFragment = new NuovoClienteFragment();
        clienteSelezzionatoFragment.setArguments(args);
        return clienteSelezzionatoFragment;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cliente_nuovo, container, false);

        /** form fields*/
        editTextRagioneSociale = (EditText) view.findViewById(R.id.ragione_sociale);
        editTextContattoRiferimento = (EditText) view.findViewById(R.id.contatto_riferimento);
        editTextNumeroContatto = (EditText) view.findViewById(R.id.numero_contatto);
        editTextEmail = (EditText) view.findViewById(R.id.contatto_email);
        editTextPartitaIva = (EditText) view.findViewById(R.id.partita_iva);
        editTextSedeOperativa = (EditText) view.findViewById(R.id.sede_operativa);

        mLoginFormView = view.findViewById(R.id.cliente_form);
        mProgressView = view.findViewById(R.id.customer_progress);

        Button buttonOk = (Button) view.findViewById(R.id.button_nuovo_cliente_salva);


        buttonOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                newCustomer();
                showProgress(true);
            }});
        return view;
    }

    private void newCustomer() {

        ragioneSociale = editTextRagioneSociale.getText().toString().trim();
        contattoRiferimento = editTextContattoRiferimento.getText().toString().trim();
        numeroContatto = editTextNumeroContatto.getText().toString().trim();
        email = editTextEmail.getText().toString().trim();
        partitaIva = editTextPartitaIva.getText().toString().trim();
        sedeOperativa = editTextSedeOperativa.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, NEW_CUSTOMER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("CUSTOMER CREATED")){
                            getFragmentManager().popBackStackImmediate();

                        }else{
                            Toast.makeText(getActivity(),"Impossibile creare cliente nuovo",Toast.LENGTH_SHORT ).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(),"Impossibile creare cliente nuovo",Toast.LENGTH_SHORT ).show();
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put(UID,"9876543210");
                map.put(DIRECTORY_ID,"5");
                map.put(DISPLAY_NAME,ragioneSociale);
                map.put(STATUS,"OK");
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
