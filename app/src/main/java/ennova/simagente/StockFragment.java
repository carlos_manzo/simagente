package ennova.simagente;

import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import ennova.simagente.MyExpandableListViewAdapter.ItemInfo;

public class StockFragment extends Fragment{
    private ExpandableListView listView;
    private MyExpandableListViewAdapter adapter;
    public static StockFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        StockFragment firstFragment = new StockFragment();
        firstFragment.setArguments(args);
        return firstFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stock_fragment, container, false);

        listView = (ExpandableListView)view.findViewById(R.id.list);
        listView.setDivider(null);
        adapter = new MyExpandableListViewAdapter(getActivity());
        listView.setAdapter(adapter);
        listView.setItemsCanFocus(true);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        Button buttonCliente = (Button) view.findViewById(R.id.buttonCliente);
        buttonCliente.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                StringBuilder sb = new StringBuilder();
                List<ItemInfo> checkedItems =adapter.getCheckedItems();
                for (ItemInfo item: checkedItems) {
                    sb.append("group[").append(item.groupPosition).append("][").append(item.childPosition).append("]-").append(item.itemContent).append("\n");
                }
                sb.append(" are checked");
                Toast.makeText(getActivity(), sb.toString(), Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }
}
