package ennova.simagente;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static ennova.simagente.LoginActivity.TOKEN_KEY;
import static ennova.simagente.LoginActivity.URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientiScannerFragment extends Fragment {

    private String CUSTOMER_URL = URL + "service/apiv1/customer?access_token=" + TOKEN_KEY;

    private ListView listView;

    private SectionListAdapter adapter;

    HashMap<String, ArrayList<String>> dataMap = new HashMap<>();
    ArrayList<String> headerList = new ArrayList<>();
    ArrayList<String> aList =  new ArrayList<>();
    ArrayList<String> bList =  new ArrayList<>();
    ArrayList<String> cList =  new ArrayList<>();
    //ArrayList<String> customerList =null;
    List<String> cleanHeadersList = null;


    public static ClientiScannerFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        ClientiScannerFragment thirdFragment = new ClientiScannerFragment();
        thirdFragment.setArguments(args);
        return thirdFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.clienti_scanner, container, false);
        listView = (ListView) view.findViewById(R.id.list_view);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Seleziona cliente");
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, CUSTOMER_URL,null,
                new Response.Listener<JSONObject>() {
                    ArrayList<String> customerList =  new ArrayList<>();
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONArray myCustomer = response.getJSONArray("detail");
                            /** looping through All Customers */
                            for (int i = 0; i < myCustomer.length(); i++) {
                                JSONObject c = myCustomer.getJSONObject(i);

                                /** detail node is JSON Object */
                                //String label = c.getString("label");
                                String display_name = c.getString("label");

                                /** adding serial number to listDataChild */
                                customerList.add(display_name);
                            }
                            /** Header, Child data */
                            createData(customerList);
                            adapter = new SectionListAdapter(getActivity());

                            for(int i = 0; i < cleanHeadersList.size(); i++){
                                if(adapter != null){
                                    adapter.addSection(
                                            headerList.get(i),
                                            dataMap.get(headerList.get(i))
                                    );
                                }
                            }

                            listView.setAdapter(adapter);


                            // TODO webservice call when open activity
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(),"Impossibile aggiornare i clienti",Toast.LENGTH_SHORT ).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(),"Impossibile aggiornare i clienti",Toast.LENGTH_SHORT ).show();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.getActivity());
        requestQueue.add(jsonRequest);





        EditText inputSearch = (EditText) view.findViewById(R.id.search_text);

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position,
                                    long id) {
                if(!adapter.isHeader(adapter.getItem(position).toString())) {
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();

                    alertDialog.setTitle("Consegna al cliente");
                    alertDialog.setMessage("Le SIM saranno consegnate al cliente " + adapter.getItem(position).toString());

                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);


                    alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            /** Create new fragment and transaction */
                            Fragment newFragment = new ClienteSelezionatoFragment();
                            /** consider using Java coding conventions (upper first char class names!!!) */
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            /** sending arguments to another fragment*/
                            Bundle args = new Bundle();
                            args.putString("cliente", adapter.getItem(position).toString());
                            newFragment.setArguments(args);
                            /** Replace whatever is in the fragment_container view with this fragment,
                             * and add the transaction to the back stack */
                            transaction.replace(R.id.container, newFragment);
                            transaction.addToBackStack(null);
                            /** Commit the transaction */
                            transaction.commit();
                        }});

                    alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Annulla", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if ( getFragmentManager().getBackStackEntryCount() > 0)
                            {
                                getFragmentManager().popBackStack();
                                return;
                            }
                        } });
                    alertDialog.show();
                }
            }
        });
        /** Inflate the layout for this fragment */
        return view;
    }

    public void createData(ArrayList<String> customerList){
        dataMap = new HashMap<>();
        headerList = new ArrayList<>();
        aList =  new ArrayList<>();
        bList =  new ArrayList<>();
        cList =  new ArrayList<>();

        for(int l=0;l<customerList.size(); l++){
            String s = customerList.get(l);
            String firstLetter = s.substring(0,1);
            headerList.add(firstLetter);
        }

        cleanHeadersList = headerList;
        Set<String> hs = new HashSet<>();
        hs.addAll(cleanHeadersList);
        cleanHeadersList.clear();
        cleanHeadersList.addAll(hs);
        Collections.sort(cleanHeadersList);

        for(int n = 0; n < cleanHeadersList.size(); n++){
            ArrayList cleanCustomerList = new ArrayList<>();
            for(int p=0;p<customerList.size(); p++){
                String sCustomer = customerList.get(p);
                String firstLetterCustomer = sCustomer.substring(0,1);
                String indexHeader = cleanHeadersList.get(n);
                if(firstLetterCustomer.equals(indexHeader)){
                    cleanCustomerList.add(sCustomer);
                }
            }
            dataMap.put(cleanHeadersList.get(n), cleanCustomerList);
        }



        for(String key : dataMap.keySet()){
            Collections.sort(dataMap.get(key));
        }
    }

}
