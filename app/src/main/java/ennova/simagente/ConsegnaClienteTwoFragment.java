package ennova.simagente;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class ConsegnaClienteTwoFragment extends Fragment {
    String[] sim = new String[]{
            "8939104700102037071",
            "8939104700102037072",
            "8939104700102037073",
            "8939104700102037074",
            "8939104700102037075",
            "8939104700102037076",
            "8939104700102037077",
            "8939104700102037078",
            "8939104700102037079",
            "8939104700102037080"
    };

    public static ConsegnaClienteTwoFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        ConsegnaClienteTwoFragment consegnaClienteFragment = new ConsegnaClienteTwoFragment();
        consegnaClienteFragment.setArguments(args);
        return consegnaClienteFragment;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.consegna_cliente, container, false);

        // The checkbox for the each item is specified by the layout android.R.layout.simple_list_item_multiple_choice
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this.getActivity(), android.R.layout.simple_list_item_multiple_choice, sim);

        // Getting the reference to the listview object of the layout
        ListView listView = (ListView) view.findViewById(R.id.lvExp1);

        // Setting adapter to the listview
        listView.setAdapter(adapter);

        Button buttonOk = (Button) view.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

            }});
        Button buttonAnulla = (Button) view.findViewById(R.id.buttonAnulla);
        buttonAnulla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if ( getFragmentManager().getBackStackEntryCount() > 0)
                {
                    getFragmentManager().popBackStack();
                    return;
                }

            }});

        return view;
    }
}
