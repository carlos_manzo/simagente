package ennova.simagente;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.ncapdevi.fragnav.FragNavController;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

import java.util.ArrayList;
import java.util.List;

import static ennova.simagente.LoginActivity.USER;

public class MainActivity extends AppCompatActivity {
    protected OnBackPressedListener onBackPressedListener;
    private BottomBar mBottomBar;
    private FragNavController fragNavController;

    /**
     * fragments index
     */
    private final int GIACENZA = FragNavController.TAB1;
    private final int LETTORE = FragNavController.TAB2;
    private final int CLIENTI = FragNavController.TAB3;
    private final int SEGNALAZIONE = FragNavController.TAB4;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Benvenuto " + USER);

        /** FragNav list of fragments */
        List<Fragment> fragments = new ArrayList<>(4);

        /** add fragments to list */
        fragments.add(GiacenzaFragment.newInstance(0));
        fragments.add(LettoreFragment.newInstance(0));
        fragments.add(ClientiFragment.newInstance(0));
        fragments.add(MessageFragment.newInstance(0));

        /** link fragments to container */
        fragNavController = new FragNavController(getSupportFragmentManager(),R.id.container,fragments);
        /** End of FragNav */

        /** BottomBar menu  */
        mBottomBar = BottomBar.attach(this, savedInstanceState);
        mBottomBar.setItems(R.menu.bottombar_menu);
        mBottomBar.setOnMenuTabClickListener(new OnMenuTabClickListener() {
            /**
             *
             * @param menuItemId
             */
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
                /** switch between tabs */
                switch (menuItemId) {
                    case R.id.bottomBarItemOne:
                        fragNavController.switchTab(GIACENZA);
                        mBottomBar.getBar().setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));
                        break;
                    case R.id.bottomBarItemSecond:
                        fragNavController.switchTab(LETTORE);
                        mBottomBar.getBar().setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));
                        break;
                    case R.id.bottomBarItemThird:
                        fragNavController.switchTab(CLIENTI);
                        mBottomBar.getBar().setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));
                        break;
                    case R.id.bottomBarItemFour:
                        fragNavController.switchTab(SEGNALAZIONE);
                        mBottomBar.getBar().setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));
                        break;
                }
            }

            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.bottomBarItemOne) {
                    fragNavController.clearStack();
                }
            }
        });
        /** End of BottomBar menu */

    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null)
            onBackPressedListener.doBack();
        else
            super.onBackPressed();
    }

    /**
     *
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        /** Necessary to restore the BottomBar's state,
         * otherwise we would lose the current tab on orientation change.
         */
        mBottomBar.onSaveInstanceState(outState);
    }

    /**
     *
     * @param title
     */
    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    /**
     *
     * @param onBackPressedListener
     */
    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }
}
