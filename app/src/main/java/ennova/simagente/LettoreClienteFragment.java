package ennova.simagente;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class LettoreClienteFragment extends Fragment {
    ArrayList simScannedList = new ArrayList<>();

    public static LettoreFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        LettoreFragment stepThreeSimInCarico = new LettoreFragment();
        stepThreeSimInCarico.setArguments(args);
        return stepThreeSimInCarico;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.step_three, container, false);
        simScannedList = getArguments().getStringArrayList("simSelectedOnGiacenza");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Conferma selezione");
// The checkbox for the each item is specified by the layout android.R.layout.simple_list_item_multiple_choice
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this.getActivity(), android.R.layout.simple_list_item_multiple_choice, simScannedList);

        // Getting the reference to the listview object of the layout
        ListView listView = (ListView) view.findViewById(R.id.lvExp1);

        // Setting adapter to the listview
        listView.setAdapter(adapter);

        Button buttonOk = (Button) view.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /** Create new fragment and transaction */
                Fragment newFragment = new ClientiScannerFragment();
                /** consider using Java coding conventions (upper first char class names!!!) */
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                /** Replace whatever is in the fragment_container view with this fragment,
                 * and add the transaction to the back stack */
                transaction.replace(R.id.container, newFragment);
                transaction.addToBackStack(null);
                /** Commit the transaction */
                transaction.commit();
            }});
        Button buttonAnulla = (Button) view.findViewById(R.id.buttonAnulla);
        buttonAnulla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if ( getFragmentManager().getBackStackEntryCount() > 0)
                {
                    getFragmentManager().popBackStack();
                    return;
                }

            }});

        return view;

    }
}
