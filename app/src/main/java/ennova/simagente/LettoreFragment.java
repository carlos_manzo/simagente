package ennova.simagente;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.google.zxing.integration.android.IntentIntegrator;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static ennova.simagente.GiacenzaFragment.simListLettoreObject;

public class LettoreFragment extends Fragment {

    public static HashMap<String, String> simListConsegnaObject;
    public static int REQUEST_CAMERA = 42;
    private static final String TAG = LettoreFragment.class.getSimpleName();
    private FrameLayout lettoreFragment;
    private DecoratedBarcodeView barcodeView;
    private BeepManager beepManager;
    private String lastText;
    private Button confermaServer;
    private Button consegna;
    private ArrayList<String> simRimasteConsegnaCliente = new ArrayList<>();

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            String match = result.getText().substring(0, Math.min(result.getText().length(), 6));
            String simVodafone = result.getText();

            Boolean itsMine = simListLettoreObject.containsKey(simVodafone);
            String status = simListLettoreObject.get(simVodafone);
            if(result.getText() == null || result.getText().equals(lastText)) {
                /** Prevent duplicate scans */
                return;
            }else if(!"893910".equals(match) || (!"19".equals(String.valueOf(simVodafone.length())))){
                /** Prevent NO vodafone scans */
                lettoreFragment.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                return;
            }else if(!itsMine){
                /** Prevent NOT MY SIM */
                lettoreFragment.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                return;
            }else{
                simListConsegnaObject.put(simVodafone, status);
                confermaServer.setVisibility(View.VISIBLE);
                consegna.setVisibility(View.VISIBLE);
                lettoreFragment.setBackgroundColor(getResources().getColor(R.color.GREEN));

            }

            lastText = result.getText();
            barcodeView.setStatusText(result.getText());
            beepManager.playBeepSoundAndVibrate();

            /** Added preview of scanned barcode*/
            ImageView imageView = (ImageView) getActivity().findViewById(R.id.barcodePreview);
            imageView.setImageBitmap(result.getBitmapWithResultPoints(Color.YELLOW));
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
    };

    public static LettoreFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        LettoreFragment secondFragment = new LettoreFragment();
        secondFragment.setArguments(args);
        return secondFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lettore, container, false);
        ((MainActivity) getActivity()).setActionBarTitle("Lettore codici a barra");
        setHasOptionsMenu(true);
        ((MainActivity)getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()){
            public void doBack() {

            }});
        lettoreFragment = (FrameLayout) view.findViewById(R.id.lettore_fragment) ;
        barcodeView = (DecoratedBarcodeView) view.findViewById(R.id.barcode_scanner);
        barcodeView.decodeContinuous(callback);
        beepManager = new BeepManager(getActivity());
        simListConsegnaObject = new HashMap<>();
        confermaServer = (Button) view.findViewById(R.id.conferma_server);
        confermaServer.setVisibility(View.GONE);
        confermaServer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /** The app has already founded NEW SIM so we address you to step one
                 * to decide if you want to take them, other wise the scanned SIM
                 * were in your possess already and we will take you to step two
                 * to decide which ones you want deliver to customer
                 * */
                /** Create new fragment and transaction */
                Fragment newFragment = new LettoreConfermaSimFragment();
                /** consider using Java coding conventions (upper first char class names!!!) */
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                /** sending arguments to another fragment*/
                Bundle args = new Bundle();
                args.putStringArrayList("simRimasteConsegnaCliente", simRimasteConsegnaCliente);
                args.putSerializable("simListConsegnaObject", simListConsegnaObject);
                newFragment.setArguments(args);
                /** Replace whatever is in the fragment_container view with this fragment,
                 * and add the transaction to the back stack */
                transaction.replace(R.id.container, newFragment);
                transaction.addToBackStack(null);
                /** Commit the transaction */
                transaction.commit();
            }
        });
        consegna = (Button) view.findViewById(R.id.consegna);
        consegna.setVisibility(View.GONE);
        consegna.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                /** Create new fragment and transaction */
                Fragment newFragment = new LettoreConsegnaFragment();
                /** consider using Java coding conventions (upper first char class names!!!) */
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                /** sending arguments to another fragment*/
                Bundle args = new Bundle();
                args.putSerializable("simListConsegnaObject", simListConsegnaObject);
                newFragment.setArguments(args);
                /** Replace whatever is in the fragment_container view with this fragment,
                 * and add the transaction to the back stack */
                transaction.replace(R.id.container, newFragment);
                transaction.addToBackStack(null);
                /** Commit the transaction */
                transaction.commit();
            }
        });

        /** Inflate the layout for this fragment */
        return view;
    }

    public void scanFromFragment() {
        IntentIntegrator.forSupportFragment(this).initiateScan();
    }

    public void onStart(){
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Lettore codici a barra");
        String cameraPermission = Manifest.permission.CAMERA;
        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), cameraPermission);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            barcodeView.resume();
        } else {
            requestPermissions(new String[]{cameraPermission}, REQUEST_CAMERA);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        barcodeView.pause();
    }

    public void triggerScan(View view) {
        barcodeView.decodeSingle(callback);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CAMERA) {
            for (int i = 0; i < permissions.length; i++) {
                if (permissions[i].equals(Manifest.permission.CAMERA)
                        && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    barcodeView.resume();
                }
            }
        }
    }

    /**
     *
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.logout_menu, menu);
    }

    /**
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /**  Handle item selection */
        switch (item.getItemId()) {
            case R.id.logout_menu:
                getActivity().finish();
                Intent intent = new Intent(this.getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
