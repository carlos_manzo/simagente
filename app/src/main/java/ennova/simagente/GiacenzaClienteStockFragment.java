package ennova.simagente;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static ennova.simagente.LoginActivity.TOKEN_KEY;
import static ennova.simagente.LoginActivity.URL;

public class GiacenzaClienteStockFragment extends Fragment {
    private String UID = null;
    private String STOCK_URL = null;
    private String cliente = null;

    ExpListViewAdapterWithCheckboxGiacenza adapterGiacenza;
    private HashMap<String, List<String>> listDataChildGiacenza;
    private ArrayList<String> cleanHeadersListGiacenza = null;
    ExpandableListView listView;
    private ImageView navReload;
    private TextView status;
    private SwipeRefreshLayout swipeContainer;
    private View mProgressView;

    /**
     *
     * @param instance
     * @return
     */
    public static GiacenzaClienteStockFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        GiacenzaClienteStockFragment giacenzaClienteStockFragment = new GiacenzaClienteStockFragment();
        giacenzaClienteStockFragment.setArguments(args);
        return giacenzaClienteStockFragment;
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.giacenza_cliente_stock, container, false);
        ((MainActivity) getActivity()).setActionBarTitle("Stock");
        ((MainActivity)getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()){
            public void doBack() {
                /** Create new fragment and transaction */
                Fragment newFragment = new GiacenzaFragment();
                /** consider using Java coding conventions (upper first char class names!!!) */
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                /** Replace whatever is in the fragment_container view with this fragment,
                 * and add the transaction to the back stack */
                transaction.replace(R.id.container, newFragment);
                transaction.addToBackStack(null);
                /** Commit the transaction */
                transaction.commit();
            }});
        Bundle b = this.getArguments();
        UID = b.getString("piva");
        mProgressView = view.findViewById(R.id.stock_progress_giacenza_stock);
        navReload = (ImageView) view.findViewById(R.id.imageViewReload_stock);
        STOCK_URL = URL +
                "service/apiv1/stockCustomer/" + UID + "?access_token=" + TOKEN_KEY;
        listView = (ExpandableListView) view.findViewById(R.id.lvExpGiacenza_stock);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, STOCK_URL,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ArrayList<String> simList =  new ArrayList<>();
                        ArrayList<String> simHeaderList =  new ArrayList<>();
                        try {
                            JSONArray mySims = response.getJSONArray("detail");
                            for (int i = 0; i < mySims.length(); i++) {
                                JSONObject c = mySims.getJSONObject(i);
                                String type = c.getString("type");
                                simHeaderList.add(type);
                                JSONArray list = c.getJSONArray("list");
                                for (int j = 0; j < list.length(); j++){
                                    JSONObject cb = list.getJSONObject(j);
                                    String serial = cb.getString("serial_number");
                                    String status = cb.getString("status");
                                    simList.add("("+type+")"+"["+status+"]"+serial);

                                }
                                //String list = c.getString("list");
                            }
                            /** Header, Child data */
                            prepareListDataGiacenza(simHeaderList,simList);
                            adapterGiacenza = new ExpListViewAdapterWithCheckboxGiacenza(getActivity(), cleanHeadersListGiacenza, listDataChildGiacenza);
                            // setting list adapter
                            listView.setAdapter(adapterGiacenza);
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(),"Impossibile aggiornare le SIM",Toast.LENGTH_SHORT ).show();
                        }
                        showProgress(false);
                        swipeContainer.setRefreshing(false);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        navReload.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(),"Impossibile aggiornare le SIM",Toast.LENGTH_SHORT ).show();
                        showProgress(false);
                        swipeContainer.setRefreshing(false);

                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.getActivity());
        requestQueue.add(jsonRequest);
        showProgress(true);
        // Lookup the swipe container view
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer_stock);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, STOCK_URL,null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                ArrayList<String> simList =  new ArrayList<>();
                                ArrayList<String> simHeaderList =  new ArrayList<>();
                                try {
                                    JSONArray mySims = response.getJSONArray("detail");
                                    for (int i = 0; i < mySims.length(); i++) {
                                        JSONObject c = mySims.getJSONObject(i);
                                        String type = c.getString("type");
                                        simHeaderList.add(type);
                                        JSONArray list = c.getJSONArray("list");
                                        for (int j = 0; j < list.length(); j++){
                                            JSONObject cb = list.getJSONObject(j);
                                            String serial = cb.getString("serial_number");
                                            String status = cb.getString("status");
                                            simList.add("("+type+")"+"["+status+"]"+serial);

                                        }
                                        //String list = c.getString("list");
                                    }
                                    /** Header, Child data */
                                    prepareListDataGiacenza(simHeaderList,simList);
                                    /**  setting list adapter */
                                    adapterGiacenza = new ExpListViewAdapterWithCheckboxGiacenza(getActivity(), cleanHeadersListGiacenza, listDataChildGiacenza);
                                    listView.setAdapter(adapterGiacenza);
                                } catch (JSONException e) {
                                    Toast.makeText(getActivity(),"Impossibile aggiornare le SIM",Toast.LENGTH_SHORT ).show();
                                }
                                showProgress(false);
                                swipeContainer.setRefreshing(false);

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                navReload.setVisibility(View.VISIBLE);
                                Toast.makeText(getActivity(),"Impossibile aggiornare le SIM",Toast.LENGTH_SHORT ).show();
                                showProgress(false);
                                swipeContainer.setRefreshing(false);

                            }
                        }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }
                };
                navReload.setVisibility(View.GONE);
                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                requestQueue.add(jsonRequest);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_red_light);
        return view;
    }

    public void onStart(){
        super.onStart();
        Bundle b = this.getArguments();
        //UID = b.getString("piva");
        cliente = b.getString("customer");

        TextView viewStock=(TextView)getActivity().findViewById(R.id.tView_giacenza_cliente_stock);
        viewStock.setText(getString(R.string.sim_giacenza) + " a " + cliente);
    }

    /**
     * Preparing the list data
     */
    private void prepareListDataGiacenza(ArrayList<String> simHeaderList, ArrayList<String> simList) {

        listDataChildGiacenza = new HashMap<>();

        cleanHeadersListGiacenza = simHeaderList;
        Set<String> hs = new HashSet<>(simHeaderList);
        hs.addAll(cleanHeadersListGiacenza);
        cleanHeadersListGiacenza.clear();
        cleanHeadersListGiacenza.addAll(hs);
        Collections.sort(cleanHeadersListGiacenza);

        for(int n = 0; n < cleanHeadersListGiacenza.size(); n++){
            ArrayList cleanCustomerList = new ArrayList<>();
            for(int p=0;p<simList.size(); p++){
                String sCustomer = simList.get(p);
                String indexHeader = cleanHeadersListGiacenza.get(n);
                String oneMatch = sCustomer.substring(sCustomer.indexOf("(") + 1, sCustomer.indexOf(")"));
                String serialNumber = sCustomer.substring(sCustomer.lastIndexOf(")") + 1);
                if(oneMatch.equals(indexHeader)){
                    cleanCustomerList.add(serialNumber);
                }
            }
            listDataChildGiacenza.put(cleanHeadersListGiacenza.get(n), cleanCustomerList);
        }
        for(String key : listDataChildGiacenza.keySet()){
            Collections.sort(listDataChildGiacenza.get(key));
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        /** On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
         * for very easy animations. If available, use these APIs to fade-in
         * the progress spinner. */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            listView.setVisibility(show ? View.GONE : View.VISIBLE);
            listView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    listView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            /** The ViewPropertyAnimator APIs are not available, so simply show
             *  and hide the relevant UI components. */
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            listView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
