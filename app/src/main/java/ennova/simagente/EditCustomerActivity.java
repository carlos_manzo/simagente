package ennova.simagente;

import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static ennova.simagente.LoginActivity.KEY_GRANT_TYPE;
import static ennova.simagente.LoginActivity.KEY_TEST_CLIENT;
import static ennova.simagente.LoginActivity.KEY_TEST_PASS;
import static ennova.simagente.LoginActivity.TOKEN_KEY;
import static ennova.simagente.LoginActivity.URL;

public class EditCustomerActivity extends AppCompatActivity {
    private String UPDATE_CUSTOMER_URL = URL +
            "service/apiv1/customer/9876543210m?access_token=" + TOKEN_KEY;
    private String CUSTOMER_UPDATED="CUSTOMER UPDATED";
    private String RAGIONE_SOCIALE = "ragione_sociale";
    private String LABEL = "label";

    private TextView ragione_sociale=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_customer);
    }

    public void onStart() {
        super.onStart();
        ragione_sociale = (TextView) findViewById(R.id.edit_ragione_sociale);
        /**show text in the Intent object in the TextView */
        ragione_sociale.setText(getIntent().getStringExtra("editCustomer"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /** Inflate the menu; this adds items to the action bar if it is present */
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /** Handle action bar item clicks here. The action bar will
         * automatically handle clicks on the Home/Up button, so long
         * as you specify a parent activity in AndroidManifest.xml.*/
        int id = item.getItemId();
        switch (id) {
            case R.id.save_menu:
                // TODO: chiamata WS per fare update sul cliente
                updateCustomer();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setResultOkSoSecondActivityWontBeShown() {
        Intent intent = new Intent();
        if (getParent() == null) {
            setResult(CustomerSelectActivity.RESULT_OK, intent);
        } else {
            getParent().setResult(CustomerSelectActivity.RESULT_OK, intent);
        }
    }

    private void updateCustomer(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPDATE_CUSTOMER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains(CUSTOMER_UPDATED)){
                            setResultOkSoSecondActivityWontBeShown();
                            finish();
                        }else{
                            Toast.makeText(EditCustomerActivity.this,"Riprova",Toast.LENGTH_LONG ).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditCustomerActivity.this,error.toString(),Toast.LENGTH_LONG ).show();
                    }
                }){

            @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put(KEY_GRANT_TYPE,"password");
                map.put("nome","");
                map.put("cognome","");
                map.put("name_referer","Android");
                map.put("email","carlos.manzo@ennova.it");
                map.put(RAGIONE_SOCIALE, ragione_sociale.getText().toString().trim());
                map.put("sede_operativa","Cagliari");
                map.put("piva","03598745434");
                map.put("cf","");
                map.put("num_tel","3247419095");
                map.put("num_cell","");
                map.put("toponomastica","");
                map.put("indirizzo","");
                map.put("provincia","");
                map.put("civico","");
                map.put("cap","");
                map.put("frazione","");
                map.put("citta","");
                map.put("id","5079");
                map.put("parent_direcotry_id","2593");//fisso e collegato al utente PIRAMIS
                map.put(LABEL, ragione_sociale.getText().toString().trim());
                map.put("uid", "9876543210m");//M5 random
                map.put("directory_id","5079");
                map.put("status","Edited from Android");

                return map;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                /*String creds = String.format("%s:%s",KEY_TEST_CLIENT,KEY_TEST_PASS);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                headers.put("Authorization", auth);*/
                headers.put("X-HTTP-Method-Override", "PATCH");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
