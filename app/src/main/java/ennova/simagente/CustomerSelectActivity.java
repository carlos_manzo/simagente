package ennova.simagente;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CustomerSelectActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_select);
        Button buttonGiacenza = (Button) findViewById(R.id.buttonGiacenzaAgenzia);
        buttonGiacenza.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //create an Intent object
                Intent intent = new Intent(getApplicationContext(), GiacenzaActivity.class);
                //add data to the Intent object
                intent.putExtra("customer", getIntent().getStringExtra("customer"));
                //start the second activity
                startActivity(intent);
                finish();
            }});
    }

    public void onStart(){
        super.onStart();
        TextView ragione_sociale=(TextView)findViewById(R.id.textViewRaggioneSocialeCliente);
        /**show text in the Intent object in the TextView */
        ragione_sociale.setText(getIntent().getStringExtra("customer"));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /** Inflate the menu; this adds items to the action bar if it is present */
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /** Handle action bar item clicks here. The action bar will
        * automatically handle clicks on the Home/Up button, so long
        * as you specify a parent activity in AndroidManifest.xml.*/
        int id = item.getItemId();
        switch (id) {
            case R.id.edit_menu:
                int REQUEST_CODE = 697;
                Intent intent = new Intent(CustomerSelectActivity.this, EditCustomerActivity.class);
                intent.putExtra("editCustomer", getIntent().getStringExtra("customer"));
                startActivityForResult(intent, REQUEST_CODE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 697) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }
}
