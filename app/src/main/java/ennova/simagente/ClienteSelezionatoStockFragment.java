package ennova.simagente;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ClienteSelezionatoStockFragment extends Fragment {

    private HashMap<String, List<String>> customerLisAllData = new HashMap<>();
    private HashMap<String, List<String>> clienteEdit = new HashMap<>();
    private List<String> clienteSelected = null;
    private String cliente = null;
    private ArrayList simRimasteConsegnaCliente = new ArrayList<>();

    public static ClienteSelezionatoStockFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        ClienteSelezionatoStockFragment clienteSelezionatoFragment = new ClienteSelezionatoStockFragment();
        clienteSelezionatoFragment.setArguments(args);
        return clienteSelezionatoFragment;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setActionBarTitle("Cliente selezionato");
        View view = inflater.inflate(R.layout.cliente_selezionato, container, false);
        setHasOptionsMenu(true);
        simRimasteConsegnaCliente = getArguments().getStringArrayList("simRimasteConsegnaCliente");


        /** SIM stock for selected customer*/
        Button buttonGiacenza = (Button) view.findViewById(R.id.buttonGiacenzaAgenzia);
        buttonGiacenza.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /** Create new fragment and transaction */
                Fragment newFragment = new GiacenzaClienteFragment();
                /** consider using Java coding conventions (upper first char class names!!!) */
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                /** sending arguments to another fragment*/
                Bundle args = new Bundle();
                args.putString("piva", clienteSelected.get(3));
                args.putString("customer", cliente);
                newFragment.setArguments(args);
                /** Replace whatever is in the fragment_container view with this fragment,
                 * and add the transaction to the back stack */
                transaction.replace(R.id.container, newFragment);
                transaction.addToBackStack(null);
                /** Commit the transaction */
                transaction.commit();
            }});
        return view;
    }

    public void onStart(){
        super.onStart();
        TextView mRagioneSociale=(TextView)getActivity().findViewById(R.id.textViewRaggioneSocialeCliente);
        TextView mContattoRiferimento=(TextView)getActivity().findViewById(R.id.textViewContattoRiferimentoCliente);
        TextView mContattoCliente=(TextView)getActivity().findViewById(R.id.textViewNumeroContattoCliente);
        TextView mEmail=(TextView)getActivity().findViewById(R.id.textViewEmailClienteCliente);
        TextView mPiva=(TextView)getActivity().findViewById(R.id.textViewPartitaIvaCliente);
        TextView mSedeOperativa=(TextView)getActivity().findViewById(R.id.textViewSedeOperativaCliente);
        Bundle b = this.getArguments();
        /**show text in the Intent object in the TextView */
        cliente = b.getString("ragione_sociale");
        mRagioneSociale.setText(cliente);
        customerLisAllData = (HashMap<String,List<String>>)b.getSerializable("customerListData");
        /** Java method .get FIND a match*/
        clienteSelected = customerLisAllData.get(b.getString("ragione_sociale"));
        mContattoRiferimento.setText(("null" != clienteSelected.get(0))? clienteSelected.get(0) : "");
        mContattoCliente.setText(("null" != clienteSelected.get(1))? clienteSelected.get(1) : "");
        mEmail.setText(("null" != clienteSelected.get(2))? clienteSelected.get(2) : "");
        mPiva.setText(("null" != clienteSelected.get(3))? clienteSelected.get(3) : "");
        mSedeOperativa.setText(("null" != clienteSelected.get(4))? clienteSelected.get(4) : "");
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.edit_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /** Handle action bar item clicks here. The action bar will
         * automatically handle clicks on the Home/Up button, so long
         * as you specify a parent activity in AndroidManifest.xml.*/
        int id = item.getItemId();
        switch (id) {
            case R.id.edit_menu:
                /** Create new fragment and transaction */
                Fragment newFragment = new ClienteEditFragment();
                /** consider using Java coding conventions (upper first char class names!!!) */
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                /** sending arguments to another fragment*/
                Bundle args = new Bundle();
                args.putSerializable("customerListData", customerLisAllData);
                args.putString("customer", cliente);
                newFragment.setArguments(args);
                /** Replace whatever is in the fragment_container view with this fragment,
                 * and add the transaction to the back stack */
                transaction.replace(R.id.container, newFragment);
                transaction.addToBackStack(null);
                /** Commit the transaction */
                transaction.commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        /** Create new fragment and transaction */
        Fragment newFragment = new GiacenzaFragment();
        /** consider using Java coding conventions (upper first char class names!!!) */
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        /** sending arguments to another fragment*/
        //Bundle args = new Bundle();
        //args.putSerializable("customerListData", customerLisAllData);
        //args.putString("customer", cliente);
        //newFragment.setArguments(args);
        /** Replace whatever is in the fragment_container view with this fragment,
         * and add the transaction to the back stack */
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        /** Commit the transaction */
        transaction.commit();
    }
}
