package ennova.simagente;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ennova.simagente.LoginActivity.KEY_GRANT_TYPE;
import static ennova.simagente.LoginActivity.TOKEN_KEY;
import static ennova.simagente.LoginActivity.URL;


public class ClienteEditFragment extends Fragment {

    private String UPDATE_CUSTOMER_URL = null;
    private String CUSTOMER_UPDATED="CUSTOMER UPDATED";
    private String RAGIONE_SOCIALE = "ragione_sociale";
    private String LABEL = "label";
    private String clienteEdit = null;
    private String CONTATTO_RIFERIMENTO = "name_referer";
    private String EMAIL = "email";
    private String SEDE_OPERATIVA = "sede_operativa";
    private String PIVA = "piva";
    private String NUM_TEL = "num_tel";
    private String UID = "uid";
    private String DISPLAY_NAME = "display_name";


    private TextView ragione_sociale=null;
    private HashMap<String, List<String>> customerLisAllDataEdit = new HashMap<>();
    private List<String> clienteSelectedEdit = null;

    private EditText mRagioneSociale = null;
    private EditText mPiva = null;
    private EditText mEmail = null;
    private EditText mSedeOperativa = null;
    private EditText mTelefonoCliente = null;
    private EditText mContattoRiferimento = null;

    /**
     *
     * @param instance
     * @return
     */
    public static ClienteEditFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        ClienteEditFragment clienteEditFragment = new ClienteEditFragment();
        clienteEditFragment.setArguments(args);
        return clienteEditFragment;
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cliente_edit, container, false);
        setHasOptionsMenu(true);

        return view;
    }

    public void onStart(){
        super.onStart();
        mRagioneSociale=(EditText) getActivity().findViewById(R.id.edit_ragione_sociale);
        mContattoRiferimento=(EditText)getActivity().findViewById(R.id.edit_contatto_riferimento);
        mTelefonoCliente=(EditText)getActivity().findViewById(R.id.edit_numero_contatto);
        mEmail=(EditText)getActivity().findViewById(R.id.edit_contatto_email);
        mPiva=(EditText)getActivity().findViewById(R.id.edit_partita_iva);
        mSedeOperativa=(EditText)getActivity().findViewById(R.id.edit_sede_operativa);
        /** Getting arguments by bundle*/
        Bundle b = this.getArguments();
        /**show text in the Intent object in the TextView */
        clienteEdit = b.getString("customer");
        mRagioneSociale.setText(clienteEdit);
        customerLisAllDataEdit = (HashMap<String,List<String>>)b.getSerializable("customerListData");
        /** Java method .get FIND a match by key*/
        clienteSelectedEdit = customerLisAllDataEdit.get(clienteEdit);
        mContattoRiferimento.setText(("null" != clienteSelectedEdit.get(0))? clienteSelectedEdit.get(0) : "");
        mTelefonoCliente.setText(("null" != clienteSelectedEdit.get(1))? clienteSelectedEdit.get(1) : "");
        mEmail.setText(("null" != clienteSelectedEdit.get(2))? clienteSelectedEdit.get(2) : "");
        mPiva.setText(("null" != clienteSelectedEdit.get(3))? clienteSelectedEdit.get(3) : "");
        mSedeOperativa.setText(("null" != clienteSelectedEdit.get(4))? clienteSelectedEdit.get(4) : "");
        UPDATE_CUSTOMER_URL = URL +
            "service/apiv1/customer/" + mPiva.getText().toString().trim() + "?access_token=" + TOKEN_KEY;
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.save_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /** Handle action bar item clicks here. The action bar will
         * automatically handle clicks on the Home/Up button, so long
         * as you specify a parent activity in AndroidManifest.xml.*/
        int id = item.getItemId();
        switch (id) {
            case R.id.save_menu:
                updateCustomer();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateCustomer(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPDATE_CUSTOMER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains(CUSTOMER_UPDATED)){
                            /** Create new fragment and transaction */
                            Fragment newFragment = new ClienteSelezionatoQueryFragment();
                            /** consider using Java coding conventions (upper first char class names!!!) */
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            /** sending arguments to another fragment*/
                            Bundle args = new Bundle();
                            args.putSerializable("ragione_sociale", mRagioneSociale.getText().toString().trim());
                            args.putSerializable("contattoRiferimento", mContattoRiferimento.getText().toString().trim());
                            args.putSerializable("numero", mTelefonoCliente.getText().toString().trim());
                            args.putSerializable("email", mEmail.getText().toString().trim());
                            args.putSerializable("piva", mPiva.getText().toString().trim());
                            args.putSerializable("sede", mSedeOperativa.getText().toString().trim());
                            args.putString("customer", mRagioneSociale.getText().toString().trim());
                            newFragment.setArguments(args);
                            /** Replace whatever is in the fragment_container view with this fragment,
                             * and add the transaction to the back stack */
                            transaction.replace(R.id.container, newFragment);
                            transaction.addToBackStack(null);
                            /** Commit the transaction */
                            transaction.commit();
                        }else{
                            Toast.makeText(getActivity(),"Impossibile aggiornare il cliente",Toast.LENGTH_SHORT ).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(),"Impossibile aggiornare il cliente",Toast.LENGTH_SHORT ).show();
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put(KEY_GRANT_TYPE,"password");// necessary for webservice
                //map.put("nome","");
                //map.put("cognome","");
                map.put(CONTATTO_RIFERIMENTO, mContattoRiferimento.getText().toString().trim());// necessary for webservice
                map.put(EMAIL, mEmail.getText().toString().trim());// necessary for webservice
                map.put(RAGIONE_SOCIALE, mRagioneSociale.getText().toString().trim());// necessary for webservice
                map.put(SEDE_OPERATIVA, mSedeOperativa.getText().toString().trim());// necessary for webservice
                map.put(PIVA,mPiva.getText().toString().trim());// necessary for webservice
                //map.put("cf","");
                map.put(NUM_TEL, mTelefonoCliente.getText().toString().trim());// necessary for webservice
                //map.put("num_cell","");
                //map.put("toponomastica","");
                //map.put("indirizzo","");
                //map.put("provincia","");
                //map.put("civico","");
                //map.put("cap","");
                //map.put("frazione","");
                //map.put("citta","");
                //map.put("id","");
                //map.put("parent_direcotry_id","2593");//generated and fixed to PIRAMIS
                map.put(DISPLAY_NAME, mRagioneSociale.getText().toString().trim());// necessary for webservice
                map.put(LABEL, mRagioneSociale.getText().toString().trim());// necessary for webservice
                map.put(UID, mPiva.getText().toString().trim());
                //map.put("directory_id","5085");generated and fixed to customer
                map.put("status","OK");

                return map;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("X-HTTP-Method-Override", "PATCH");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }
}
