package ennova.simagente;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import static ennova.simagente.LoginActivity.TOKEN_KEY;
import static ennova.simagente.LoginActivity.USER;
import static ennova.simagente.LoginActivity.URL;

public class NewMessageActivity extends AppCompatActivity {

    private String MESSAGE_URL = URL + "service/apiv1/message?access_token=" + TOKEN_KEY;

    private String SUBJECT="subject";
    private String MESSAGE= "message";
    private String FROM= "from";

    private EditText editTextOggetto;
    private EditText editTextSegnalazione;
    private View mProgressView;
    private View mMessageFormView;
    private String oggetto;
    private String segnalazione;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);
        setTitle("Crea segnalazione");

        /** form fields*/
        editTextOggetto = (EditText) findViewById(R.id.subject_message);
        editTextSegnalazione = (EditText) findViewById(R.id.text_mesage);
        mMessageFormView = findViewById(R.id.new_message_form);
        mProgressView = findViewById(R.id.customer_progress);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /** Inflate the menu; this adds items to the action bar if it is present */
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_message_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /** Handle action bar item clicks here. The action bar will
         * automatically handle clicks on the Home/Up button, so long
         * as you specify a parent activity in AndroidManifest.xml.*/
        int id = item.getItemId();
        switch (id) {
            case R.id.send_message_menu:
                newMessage();
                showProgress(true);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void newMessage() {

        oggetto = editTextOggetto.getText().toString().trim();
        segnalazione = editTextSegnalazione.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, MESSAGE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("Messages")){
                            finish();
                            Toast.makeText(NewMessageActivity.this,"Segnalazione inviata",Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(NewMessageActivity.this,"Segnalazione non riuscita",Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(NewMessageActivity.this,error.toString(),Toast.LENGTH_LONG ).show();
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put(SUBJECT,oggetto);
                map.put(MESSAGE,segnalazione);
                map.put(FROM,USER);
                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mMessageFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mMessageFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mMessageFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mMessageFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
