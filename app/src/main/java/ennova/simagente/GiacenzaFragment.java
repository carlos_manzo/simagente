package ennova.simagente;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static ennova.simagente.ExpListViewAdapterWithCheckbox.simChecked;
import static ennova.simagente.LoginActivity.KEY_GRANT_TYPE;
import static ennova.simagente.LoginActivity.TOKEN_KEY;
import static ennova.simagente.LoginActivity.USER;
import static ennova.simagente.LoginActivity.URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class GiacenzaFragment extends Fragment {

    public static HashMap<String, List<String>> listDataChild;
    public static HashMap<String, String> simListLettoreObject;
    public static ArrayList<String> simList;
    public static ArrayList<String> simListLettore = new ArrayList<>();

    private String GIACENZA_URL = URL +
            "service/apiv1/item?access_token=" + TOKEN_KEY;
    private String CONSEGNA_AGENZIA_URL = URL +
            "service/apiv1/movement?access_token=" + TOKEN_KEY;

    /** Notifications*/
    private String MESSAGE_URL = URL + "service/apiv1/message?access_token=" + TOKEN_KEY;
    private HashMap<String, List<String>> listDataChildMessage;
    private ArrayList<String> cleanHeadersListMessage = null;
    /** Notifications end */

    ExpListViewAdapterWithCheckbox adapter;
    ExpandableListView listView;
    ArrayList<String> cleanHeadersList = null;
    ArrayList simSelectedAgenzia = new ArrayList<>();
    private List<String> mItems;
    private int hot_number = 0;
    private TextView ui_hot = null;

    private View mProgressView;

    public static GiacenzaFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        GiacenzaFragment firstFragment = new GiacenzaFragment();
        firstFragment.setArguments(args);
        return firstFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.giacenza, container, false);
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).setActionBarTitle("Benvenuto "+ USER);
        ((MainActivity)getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()){
            public void doBack() {

        }});

        listView = (ExpandableListView) view.findViewById(R.id.lvExp);
        mItems = new ArrayList<>();
        mProgressView = view.findViewById(R.id.stock_progress);
        getNotifications();
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, GIACENZA_URL,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        simListLettoreObject = new HashMap<>();
                        simList =  new ArrayList<>();
                        ArrayList<String> simHeaderList =  new ArrayList<>();
                        simListLettore = new ArrayList<>();
                        try {
                            JSONArray mySims = response.getJSONArray("detail");
                            /** looping through All Sim */
                            for (int i = 0; i < mySims.length(); i++) {
                                JSONObject c = mySims.getJSONObject(i);

                                /** detail node is JSON Object */
                                String label = c.getString("label");
                                String serial_number = c.getString("serial_number");
                                String status = c.getString("status");

                                /** adding serial number to listDataChild */
                                simHeaderList.add(label);
                                simList.add("("+label+")"+serial_number);

                                /** adding serialnumer and status for lettore codice a barra*/
                                //simListLettore.add("("+status+")"+serial_number);
                                simListLettoreObject.put(serial_number,status);
                            }
                            /** Header, Child data */
                            prepareListData(simHeaderList,simList);
                            adapter = new ExpListViewAdapterWithCheckbox(getActivity(), cleanHeadersList, listDataChild);
                            /**  setting list adapter **/
                            listView.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        showProgress(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(),"Impossibile aggiornare le SIM",Toast.LENGTH_SHORT ).show();
                        showProgress(false);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonRequest);
        showProgress(true);

        /** Listview on child click listener */
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                Toast.makeText(
                        getActivity(),
                        cleanHeadersList.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                cleanHeadersList.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });

        Button buttonAgenzia = (Button) view.findViewById(R.id.buttonAgenzia);
        buttonAgenzia.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(!simChecked.isEmpty()){
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Consegna in agenzia");
                    alertDialog.setMessage("Sicuro di voler consegnare le SIM selezionate in agenzia?");
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            StringRequest stringRequestConsegnaAgenzia = new StringRequest(Request.Method.POST, CONSEGNA_AGENZIA_URL,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            if(response.contains("Movements")){
                                                AlertDialog alertDialogo = new AlertDialog.Builder(getActivity()).create();
                                                alertDialogo.setMessage("SIM consegnate correttamente!");
                                                alertDialogo.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        /** Create new fragment and transaction */
                                                        Fragment newFragment = new GiacenzaFragment();
                                                        /** consider using Java coding conventions (upper first char class names!!!) */
                                                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                                        /** sending arguments to another fragment*/
                                                        //Bundle args = new Bundle();
                                                        //args.putStringArrayList("simSelected", simChecked);
                                                        //newFragment.setArguments(args);
                                                        /** Replace whatever is in the fragment_container view with this fragment,
                                                         * and add the transaction to the back stack */
                                                        transaction.replace(R.id.container, newFragment);
                                                        transaction.addToBackStack(null);
                                                        /** Commit the transaction */
                                                        transaction.commit();
                                                    }});
                                                alertDialogo.show();
                                            }else{
                                                Toast.makeText(getActivity(),"Impossibile aggiornare le SIM",Toast.LENGTH_SHORT ).show();
                                            }
                                            showProgress(false);
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Toast.makeText(getActivity(),"Impossibile aggiornare le SIM",Toast.LENGTH_SHORT ).show();
                                            showProgress(false);
                                        }
                                    }){
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String,String> map = new HashMap<>();
                                    simSelectedAgenzia = simChecked;
                                    for(int i=0; i < simSelectedAgenzia.size(); i++) {
                                        map.put("sn[" + i + "][0]", String.valueOf(simSelectedAgenzia.get(i)));
                                        map.put("sn["+i+"][1]","ATTIVABILE");
                                    }
                                    map.put(KEY_GRANT_TYPE,"password");// necessary for webservice
                                    map.put("directory_id", "parent");
                                    return map;
                                }
                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    HashMap<String, String> headers = new HashMap<>();
                                    headers.put("X-HTTP-Method-Override", "PATCH");
                                    return headers;
                                }
                            };
                            RequestQueue requestQueueConsegnaAgenzia = Volley.newRequestQueue(getActivity());
                            requestQueueConsegnaAgenzia.add(stringRequestConsegnaAgenzia);
                            showProgress(true);
                        } });

                    alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Annulla", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if ( getFragmentManager().getBackStackEntryCount() > 0)
                            {
                                getFragmentManager().popBackStack();
                                return;
                            }
                        } });
                    alertDialog.show();
                }else{
                    Toast.makeText(getActivity(),"Selezionare almeno una SIM",Toast.LENGTH_SHORT ).show();
                }
            }
        });
        Button buttonCliente = (Button) view.findViewById(R.id.buttonCliente);
        buttonCliente.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(!simChecked.isEmpty()){
                    /** Create new fragment and transaction */
                    Fragment newFragment = new ConsegnaClienteFragment();
                    /** consider using Java coding conventions (upper first char class names!!!) */
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    /** sending arguments to another fragment*/
                    Bundle args = new Bundle();
                    args.putStringArrayList("simSelected", simChecked);
                    newFragment.setArguments(args);
                    /** Replace whatever is in the fragment_container view with this fragment,
                     * and add the transaction to the back stack */
                    transaction.replace(R.id.container, newFragment);
                    transaction.addToBackStack(null);
                    /** Commit the transaction */
                    transaction.commit();
                }else{
                    Toast.makeText(getActivity(),"Selezionare almeno una SIM",Toast.LENGTH_SHORT ).show();
                }
            }
        });
        return view;
    }

    /**
     * Preparing the list data
     */
    private void prepareListData(ArrayList<String> simHeaderList, ArrayList<String> simList) {

        listDataChild = new HashMap<>();

        cleanHeadersList = simHeaderList;
        Set<String> hs = new HashSet<>(simHeaderList);
        hs.addAll(cleanHeadersList);
        cleanHeadersList.clear();
        cleanHeadersList.addAll(hs);
        Collections.sort(cleanHeadersList);

        for(int n = 0; n < cleanHeadersList.size(); n++){
            ArrayList cleanCustomerList = new ArrayList<>();
            for(int p=0;p<simList.size(); p++){
                String sCustomer = simList.get(p);
                String indexHeader = cleanHeadersList.get(n);
                String oneMatch = sCustomer.substring(sCustomer.indexOf("(") + 1, sCustomer.indexOf(")"));
                String serialNumber = sCustomer.substring(sCustomer.lastIndexOf(")") + 1);
                if(oneMatch.equals(indexHeader)){
                    cleanCustomerList.add(serialNumber);
                }
            }
            listDataChild.put(cleanHeadersList.get(n), cleanCustomerList);
        }
        for(String key : listDataChild.keySet()){
            Collections.sort(listDataChild.get(key));
        }
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.stock_menu, menu);
        final View menu_hotlist = menu.findItem(R.id.badge).getActionView();
        ui_hot = (TextView) menu_hotlist.findViewById(R.id.hotlist_hot);
        updateHotCount(hot_number);
        new MyMenuItemStuffListener(menu_hotlist, "leggere messaggi") {
            @Override
            public void onClick(View v) {
                if(hot_number > 0){
                    /** Create new fragment and transaction */
                    Fragment newFragment = new NotificationFragment();
                    /** consider using Java coding conventions (upper first char class names!!!) */
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    /** sending arguments to another fragment*/
                    //Bundle args = new Bundle();
                    //args.putStringArrayList("simSelected", simChecked);
                    //newFragment.setArguments(args);
                    /** Replace whatever is in the fragment_container view with this fragment,
                     * and add the transaction to the back stack */
                    transaction.replace(R.id.container, newFragment);
                    //transaction.addToBackStack(null);
                    /** Commit the transaction */
                    transaction.commit();
                }else{
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setMessage("Non sono presenti messaggi");
                    alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }});
                    alertDialog.show();
                }
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /** Handle item selection */
        switch (item.getItemId()) {
            case R.id.stock_logout_menu:
                getActivity().finish();
                Intent intent = new Intent(this.getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }}
    /** call the updating code on the main thread,
     * so we can call this asynchronously
     */
    public void updateHotCount(final int new_hot_number) {
        hot_number = new_hot_number;
        if (ui_hot == null) return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (new_hot_number == 0)
                    ui_hot.setVisibility(View.INVISIBLE);
                else {
                    ui_hot.setVisibility(View.VISIBLE);
                    ui_hot.setText(Integer.toString(new_hot_number));
                }
            }
        });
    }

    abstract class MyMenuItemStuffListener implements View.OnClickListener, View.OnLongClickListener {
        private String hint;
        private View view;

        MyMenuItemStuffListener(View view, String hint) {
            this.view = view;
            this.hint = hint;
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override abstract public void onClick(View v);

        @Override public boolean onLongClick(View v) {
            final int[] screenPos = new int[2];
            final Rect displayFrame = new Rect();
            view.getLocationOnScreen(screenPos);
            view.getWindowVisibleDisplayFrame(displayFrame);
            final Context context = view.getContext();
            final int width = view.getWidth();
            final int height = view.getHeight();
            final int midy = screenPos[1] + height / 2;
            final int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
            Toast cheatSheet = Toast.makeText(context, hint, Toast.LENGTH_SHORT);
            if (midy < displayFrame.height()) {
                cheatSheet.setGravity(Gravity.TOP | Gravity.RIGHT,
                        screenWidth - screenPos[0] - width / 2, height);
            } else {
                cheatSheet.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, height);
            }
            cheatSheet.show();
            return true;
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        /** On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
         * for very easy animations. If available, use these APIs to fade-in
         * the progress spinner. */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            listView.setVisibility(show ? View.GONE : View.VISIBLE);
            listView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    listView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            /** The ViewPropertyAnimator APIs are not available, so simply show
             *  and hide the relevant UI components. */
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            listView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void getNotifications(){
        JsonObjectRequest jsonRequestNotifications = new JsonObjectRequest(Request.Method.GET, MESSAGE_URL,null,
                new Response.Listener<JSONObject>() {
                    ArrayList<String> messageList =  new ArrayList<>();
                    ArrayList<String> messageHeaderList =  new ArrayList<>();
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray myMessages = response.getJSONArray("detail");
                            /** looping through All Messages */
                            for (int i = 0; i < myMessages.length(); i++) {
                                JSONObject c = myMessages.getJSONObject(i);

                                /** detail node is JSON Object */
                                String id_message = c.getString("id");
                                String read_message = c.getString("read");
                                String text_message = c.getString("text");
                                String header = c.getString("day");

                                /** adding messages to listData */
                                // String text_message_short = text_message.substring(0, Math.min(text_message.length(), 70))+ " ...";
                                messageList.add("("+header+")"+text_message);
                                messageHeaderList.add(header);
                            }
                            /** Header, Child data */
                            prepareListDataNotifications(messageHeaderList,messageList);
                            updateHotCount(messageList.size());
                            //adapterNotifications = new ExpListViewAdapterWithCheckboxMessage(getActivity(), cleanHeadersListMessage, listDataChildMessage);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        showProgress(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        Toast.makeText(getActivity(),error.toString(),Toast.LENGTH_LONG ).show();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        RequestQueue requestQueueNotifications = Volley.newRequestQueue(getActivity());
        requestQueueNotifications.add(jsonRequestNotifications);
    }

    /**
     * Preparing the list data
     */
    private void prepareListDataNotifications(ArrayList<String> messageHeaderList, ArrayList<String> messageList) {

        listDataChildMessage = new HashMap<>();

        cleanHeadersListMessage = messageHeaderList;
        Set<String> hs = new HashSet<>(messageHeaderList);
        hs.addAll(cleanHeadersListMessage);
        cleanHeadersListMessage.clear();
        cleanHeadersListMessage.addAll(hs);
        Collections.sort(cleanHeadersListMessage,Collections.reverseOrder());

        for(int n = 0; n < cleanHeadersListMessage.size(); n++){
            ArrayList cleanMessageList = new ArrayList<>();
            for(int p=0;p<messageList.size(); p++){
                String sCustomer = messageList.get(p);
                String indexHeader = cleanHeadersListMessage.get(n);
                String oneMatch = sCustomer.substring(sCustomer.indexOf("(") + 1, sCustomer.indexOf(")"));
                String serialNumber = sCustomer.substring(sCustomer.lastIndexOf(")") + 1);
                if(oneMatch.equals(indexHeader)){
                    cleanMessageList.add(serialNumber);
                }
            }
            listDataChildMessage.put(cleanHeadersListMessage.get(n), cleanMessageList);
        }
        for(String key : listDataChildMessage.keySet()){
            Collections.sort(listDataChildMessage.get(key),Collections.reverseOrder());
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Benvenuto "+ USER);
    }
}
