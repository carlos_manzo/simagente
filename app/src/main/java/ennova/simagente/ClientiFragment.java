package ennova.simagente;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static ennova.simagente.LoginActivity.TOKEN_KEY;
import static ennova.simagente.LoginActivity.URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientiFragment extends Fragment {

    private String CUSTOMER_URL = URL +
            "service/apiv1/customer?access_token=" + TOKEN_KEY;

    private ListView listView;
    private SectionListAdapter adapter;
    private EditText inputSearch;

    HashMap<String, ArrayList<String>> dataMap = new HashMap<>();
    ArrayList<String> headerList = new ArrayList<>();
    List<String> cleanHeadersList = null;
    /** Create a HashMap of customers and their data. */
    private HashMap<String, List<String>> customerLisData = new HashMap<>();
    private View mProgressView;

    public static ClientiFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        ClientiFragment clientiFragment = new ClientiFragment();
        clientiFragment.setArguments(args);
        return clientiFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.clienti, container, false);
        ((MainActivity) getActivity()).setActionBarTitle("Anagrafica clienti");
        setHasOptionsMenu(true);
        ((MainActivity)getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()){
            public void doBack() {

            }});
        listView = (ListView) view.findViewById(R.id.list_view_clienti);

        mProgressView = view.findViewById(R.id.customer_list_progress);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, CUSTOMER_URL,null,
                new Response.Listener<JSONObject>() {
                    ArrayList<String> customerList =  new ArrayList<>();
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONArray myCustomer = response.getJSONArray("detail");
                            /** looping through All Customers */
                            for (int i = 0; i < myCustomer.length(); i++) {
                                JSONObject c = myCustomer.getJSONObject(i);

                                /** detail node is JSON Object */
                                String display_name = c.getString("label");
                                String name_referer = c.getString("name_referer");
                                String num_tel = c.getString("num_tel");
                                String email = c.getString("email");
                                String piva = c.getString("piva");
                                String sede_operativa = c.getString("sede_operativa");

                                /** adding display name to listDataChild */
                                customerList.add(display_name);
                                customerLisData.put(display_name,
                                        new ArrayList<>(Arrays.asList(
                                                name_referer,
                                                num_tel,
                                                email,
                                                piva,
                                                sede_operativa)));
                            }
                            /** Header, Child data */
                            adapter = new SectionListAdapter(getActivity());
                            if(!customerList.isEmpty()){
                                createData(customerList);
                                for(int i = 0; i < cleanHeadersList.size(); i++){
                                    if(adapter != null){
                                        adapter.addSection(
                                                headerList.get(i),
                                                dataMap.get(headerList.get(i))
                                        );
                                    }
                                }
                            }
                            listView.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        showProgress(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(),"Impossibile aggiornare i clienti",Toast.LENGTH_LONG ).show();
                        showProgress(false);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.getActivity());
        requestQueue.add(jsonRequest);
        showProgress(true);

        inputSearch = (EditText) view.findViewById(R.id.search_text);

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                if(!adapter.isHeader(adapter.getItem(position).toString())) {
                    String selectedCustomer = adapter.getItem(position).toString();
                    /** Create new fragment and transaction */
                    Fragment newFragment = new ClienteSelezionatoFragment();
                    /** consider using Java coding conventions (upper first char class names!!!) */
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    /** sending arguments to another fragment*/
                    Bundle args = new Bundle();
                    args.putString("ragione_sociale", selectedCustomer);
                    args.putSerializable("customerListData", customerLisData);
                    newFragment.setArguments(args);
                    /** Replace whatever is in the fragment_container view with this fragment,
                     * and add the transaction to the back stack */
                    transaction.replace(R.id.container, newFragment);
                    transaction.addToBackStack("cliente");
                    /** Commit the transaction */
                    transaction.commit();

                }
            }
        });

        /**
         * New customer by floating button
         */
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.add_customer);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Create new fragment and transaction */
                Fragment newFragment = new ClienteNewFragment();
                /** consider using Java coding conventions (upper first char class names!!!) */
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                /** sending arguments to another fragment*/
                Bundle args = new Bundle();
                //args.putString("piva", clienteSelected.get(3));
                //args.putString("customer", cliente);
                newFragment.setArguments(args);
                /** Replace whatever is in the fragment_container view with this fragment,
                 * and add the transaction to the back stack */
                transaction.replace(R.id.container, newFragment);
                transaction.addToBackStack(null);
                /** Commit the transaction */
                transaction.commit();
            }
        });

        /** Inflate the layout for this fragment */
        return view;
    }

    public void createData(ArrayList<String> customerList){
        dataMap = new HashMap<>();
        headerList = new ArrayList<>();

        for(int l=0;l<customerList.size(); l++){
            String s = customerList.get(l);
            String firstLetter = s.substring(0,1);
            headerList.add(firstLetter);
        }

        cleanHeadersList = headerList;
        Set<String> hs = new HashSet<>();
        hs.addAll(cleanHeadersList);
        cleanHeadersList.clear();
        cleanHeadersList.addAll(hs);
        Collections.sort(cleanHeadersList);

        for(int n = 0; n < cleanHeadersList.size(); n++){
            ArrayList cleanCustomerList = new ArrayList<>();
            for(int p=0;p<customerList.size(); p++){
                String sCustomer = customerList.get(p);
                String firstLetterCustomer = sCustomer.substring(0,1);
                String indexHeader = cleanHeadersList.get(n);
                if(firstLetterCustomer.equals(indexHeader)){
                    cleanCustomerList.add(sCustomer);
                }
            }
            dataMap.put(cleanHeadersList.get(n), cleanCustomerList);
        }



        for(String key : dataMap.keySet()){
            Collections.sort(dataMap.get(key));
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        /** On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
         * for very easy animations. If available, use these APIs to fade-in
         * the progress spinner. */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            listView.setVisibility(show ? View.GONE : View.VISIBLE);
            listView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    listView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            /** The ViewPropertyAnimator APIs are not available, so simply show
             *  and hide the relevant UI components. */
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            listView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.logout_menu, menu);
    }

    /**
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /** Handle item selection */
        switch (item.getItemId()) {
            case R.id.logout_menu:
                getActivity().finish();
                Intent intent = new Intent(this.getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Anagrafica clienti");
    }
}
