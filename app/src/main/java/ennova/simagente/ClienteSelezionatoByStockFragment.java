package ennova.simagente;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ClienteSelezionatoByStockFragment extends Fragment {

    private HashMap<String, List<String>> customerLisAllData = new HashMap<>();
    private HashMap<String, List<String>> customerListDatas = new HashMap<>();
    private List<String> clienteSelected = null;
    private String UID = null;
    private String cliente = null;
    private TextView mRagioneSociale = null;
    private TextView mContattoRiferimento = null;
    private TextView mContattoCliente= null;
    private TextView mEmail = null;
    private TextView mPiva;
    private TextView mSedeOperativa = null;
    /** Create a HashMap of customers and their data. */
    private HashMap<String, List<String>> customerLisData = new HashMap<>();


    public static ClienteSelezionatoByStockFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        ClienteSelezionatoByStockFragment clienteSelezionatoFragment = new ClienteSelezionatoByStockFragment();
        clienteSelezionatoFragment.setArguments(args);
        return clienteSelezionatoFragment;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setActionBarTitle("Cliente selezionato");
        View view = inflater.inflate(R.layout.cliente_selezionato, container, false);
        setHasOptionsMenu(true);
        /** hiding soft keyboard*/
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        Button buttonGiacenza = (Button) view.findViewById(R.id.buttonGiacenzaAgenzia);
        Bundle b = this.getArguments();
        UID = b.getString("piva");
        buttonGiacenza.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /** Create new fragment and transaction */
                Fragment newFragment = new GiacenzaClienteStockFragment();
                /** consider using Java coding conventions (upper first char class names!!!) */
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                /** sending arguments to another fragment*/
                Bundle args = new Bundle();
                args.putString("piva", UID);
                args.putString("customer", cliente);
                newFragment.setArguments(args);
                /** Replace whatever is in the fragment_container view with this fragment,
                 * and add the transaction to the back stack */
                transaction.replace(R.id.container, newFragment);
                transaction.addToBackStack(null);
                /** Commit the transaction */
                transaction.commit();
            }});
        return view;
    }

    public void onStart(){
        super.onStart();
        TextView mRagioneSociale=(TextView)getActivity().findViewById(R.id.textViewRaggioneSocialeCliente);
        TextView mContattoRiferimento=(TextView)getActivity().findViewById(R.id.textViewContattoRiferimentoCliente);
        TextView mContattoCliente=(TextView)getActivity().findViewById(R.id.textViewNumeroContattoCliente);
        TextView mEmail=(TextView)getActivity().findViewById(R.id.textViewEmailClienteCliente);
        mPiva=(TextView)getActivity().findViewById(R.id.textViewPartitaIvaCliente);
        TextView mSedeOperativa=(TextView)getActivity().findViewById(R.id.textViewSedeOperativaCliente);
        Bundle b = this.getArguments();
        /**show text in the Intent object in the TextView */
        cliente = b.getString("ragione_sociale");
        mRagioneSociale.setText(cliente);
        customerLisData.put(cliente,
                new ArrayList<>(Arrays.asList(
                        b.getString("contattoRiferimento"),
                        b.getString("numero"),
                        b.getString("email"),
                        b.getString("piva"),
                        b.getString("sede"))));
        customerLisAllData = customerLisData;
                /** Java method .get FIND a match*/
        //clienteSelected = customerLisAllData.get(b.getString("ragione_sociale"));
        mContattoRiferimento.setText(b.getString("contattoRiferimento"));
        mContattoCliente.setText(b.getString("numero"));
        mEmail.setText(b.getString("email"));
        mPiva.setText(b.getString("piva"));
        mSedeOperativa.setText(b.getString("sede"));
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.edit_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /** Handle action bar item clicks here. The action bar will
         * automatically handle clicks on the Home/Up button, so long
         * as you specify a parent activity in AndroidManifest.xml.*/
        int id = item.getItemId();
        switch (id) {
            case R.id.edit_menu:
                /** Create new fragment and transaction */
                Fragment newFragment = new ClienteEditFragment();
                /** consider using Java coding conventions (upper first char class names!!!) */
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                /** sending arguments to another fragment*/
                Bundle args = new Bundle();
                args.putSerializable("customerListData", customerLisAllData);
                args.putString("customer", cliente);
                newFragment.setArguments(args);
                /** Replace whatever is in the fragment_container view with this fragment,
                 * and add the transaction to the back stack */
                transaction.replace(R.id.container, newFragment);
                transaction.addToBackStack(null);
                /** Commit the transaction */
                transaction.commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        /** Create new fragment and transaction */
        Fragment newFragment = new GiacenzaFragment();
        /** consider using Java coding conventions (upper first char class names!!!) */
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        /** sending arguments to another fragment*/
        //Bundle args = new Bundle();
        //args.putSerializable("customerListData", customerLisAllData);
        //args.putString("customer", cliente);
        //newFragment.setArguments(args);
        /** Replace whatever is in the fragment_container view with this fragment,
         * and add the transaction to the back stack */
        transaction.replace(R.id.container, newFragment);
        //transaction.addToBackStack(null);
        /** Commit the transaction */
        transaction.commit();
    }
}
