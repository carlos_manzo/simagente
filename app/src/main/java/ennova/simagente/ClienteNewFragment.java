package ennova.simagente;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ennova.simagente.LoginActivity.TOKEN_KEY;
import static ennova.simagente.LoginActivity.URL;

public class ClienteNewFragment extends Fragment {
    public static final String NEW_CUSTOMER_URL = URL + "service/apiv1/customer?access_token=" + TOKEN_KEY;

    private String UID              =   "uid";
    private String DIRECTORY_ID     =   "directory_id";
    private String DISPLAY_NAME     =   "display_name";
    private String STATUS           =   "status";
    private String RAGIONE_SOCIALE  =   "ragione_sociale";
    private String NAME_REFERER     =   "name_referer";
    private String NUM_TEL          =   "num_tel";
    private String EMAIL            =   "email";
    private String PIVA             =   "piva";
    private String SEDE_OPERATIVA   =   "sede_operativa";

    private EditText editTextRagioneSociale;
    private EditText editTextContattoRiferimento;
    private EditText editTextNumeroContatto;
    private EditText editTextEmail;
    private EditText editTextPartitaIva;
    private EditText editTextSedeOperativa;

    private String ragioneSociale;
    private String contattoRiferimento;
    private String numeroContatto;
    private String email;
    private String partitaIva;
    private String sedeOperativa;

    /** Create a HashMap of customers and their data. */
    private HashMap<String, List<String>> customerLisData = new HashMap<>();

    private View mProgressView;
    private View mLoginFormView;

    public static ClienteNewFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt("argsInstance", instance);
        ClienteNewFragment clienteNewFragment = new ClienteNewFragment();
        clienteNewFragment.setArguments(args);
        return clienteNewFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cliente_new_fragment, container, false);
        /** Fix Action bar title*/
        ((MainActivity) getActivity()).setActionBarTitle("Nuovo cliente");
        /** Load button menu*/
        setHasOptionsMenu(true);
        /** form fields*/
        editTextRagioneSociale = (EditText) view.findViewById(R.id.ragione_sociale);
        editTextContattoRiferimento = (EditText) view.findViewById(R.id.contatto_riferimento);
        editTextNumeroContatto = (EditText) view.findViewById(R.id.numero_contatto);
        editTextEmail = (EditText) view.findViewById(R.id.contatto_email);
        editTextPartitaIva = (EditText) view.findViewById(R.id.partita_iva);
        editTextSedeOperativa = (EditText) view.findViewById(R.id.sede_operativa);

        mLoginFormView = view.findViewById(R.id.cliente_form);
        mProgressView = view.findViewById(R.id.customer_progress);

        /** Inflate the layout for this fragment */
        return view;
    }

    private void newCustomer() {
        boolean cancel = false;
        View focusView = null;

        ragioneSociale = editTextRagioneSociale.getText().toString().trim();
        contattoRiferimento = editTextContattoRiferimento.getText().toString().trim();
        numeroContatto = editTextNumeroContatto.getText().toString().trim();
        email = editTextEmail.getText().toString().trim();
        partitaIva = editTextPartitaIva.getText().toString().trim();
        sedeOperativa = editTextSedeOperativa.getText().toString().trim();

        if (TextUtils.isEmpty(ragioneSociale)) {
            editTextRagioneSociale.setError(getString(R.string.error_field_required));
            focusView = editTextRagioneSociale;
            cancel = true;
        }else if(TextUtils.isEmpty(partitaIva)){
            editTextPartitaIva.setError(getString(R.string.error_field_required));
            focusView = editTextPartitaIva;
            cancel = true;
        }
        if (cancel) {
            /** There was an error; don't attempt to create new customer and focus the first
             * form field with an error.
             **/
            focusView.requestFocus();
        } else {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, NEW_CUSTOMER_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(response.contains("CUSTOMER CREATED")){
                                Toast.makeText(getActivity(),"Cliente aggiunto",Toast.LENGTH_LONG).show();
                                /** addressing to cliente selezionato fragment*/
                                /** Create new fragment and transaction */
                                Fragment newFragment = new ClienteSelezionatoFragment();
                                /** consider using Java coding conventions (upper first char class names!!!) */
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                /** sending arguments to another fragment*/
                                Bundle args = new Bundle();
                                customerLisData.put(ragioneSociale,
                                        new ArrayList<>(Arrays.asList(
                                                contattoRiferimento,
                                                numeroContatto,
                                                email,
                                                partitaIva,
                                                sedeOperativa)));
                                args.putSerializable("customerListData", customerLisData);
                                args.putString("ragione_sociale", ragioneSociale);
                                newFragment.setArguments(args);
                                /** Replace whatever is in the fragment_container view with this fragment,
                                 * and add the transaction to the back stack */
                                transaction.replace(R.id.container, newFragment);
                                transaction.addToBackStack(null);
                                /** Commit the transaction */
                                transaction.commit();
                            }else{
                                Toast.makeText(getActivity(),"Impossibile aggiornare il cliente",Toast.LENGTH_SHORT ).show();
                                showProgress(false);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getActivity(),"Impossibile aggiornare il cliente",Toast.LENGTH_SHORT ).show();
                            showProgress(false);
                        }
                    }){

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> map = new HashMap<>();
                    map.put(UID,partitaIva);
                    map.put(DIRECTORY_ID,"self");
                    map.put(DISPLAY_NAME,ragioneSociale);
                    map.put(STATUS,"OK");
                    map.put(RAGIONE_SOCIALE,ragioneSociale);
                    map.put(NAME_REFERER, contattoRiferimento);
                    map.put(NUM_TEL,numeroContatto);
                    map.put(EMAIL,email);
                    map.put(PIVA, partitaIva);
                    map.put(SEDE_OPERATIVA, sedeOperativa);
                    return map;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        }
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.save_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /** Handle action bar item clicks here. The action bar will
         * automatically handle clicks on the Home/Up button, so long
         * as you specify a parent activity in AndroidManifest.xml.*/
        int id = item.getItemId();
        switch (id) {
            case R.id.save_menu:
                newCustomer();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
